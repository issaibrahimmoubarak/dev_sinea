package ne.mha.sinea.nomenclature.disposition;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface DispositionRepository extends CrudRepository<Disposition, Integer> {
	Disposition findByCode(Integer code);
	Disposition findByIsDeletedFalseAndLibelle(String libelle);
	List<Disposition> findByIsDeletedFalseOrderByLibelleAsc();
	List<Disposition> findByIsDeletedFalse();
	List<Disposition> findByIsDeletedTrue();
	List<Disposition> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Disposition> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
