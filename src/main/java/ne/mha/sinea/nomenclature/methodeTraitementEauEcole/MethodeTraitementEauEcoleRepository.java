package ne.mha.sinea.nomenclature.methodeTraitementEauEcole;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface MethodeTraitementEauEcoleRepository extends CrudRepository<MethodeTraitementEauEcole, Integer> {
	MethodeTraitementEauEcole findByCode(Integer code);
	MethodeTraitementEauEcole findByIsDeletedFalseAndLibelle(String libelle);
	List<MethodeTraitementEauEcole> findByIsDeletedFalseOrderByLibelleAsc();
	List<MethodeTraitementEauEcole> findByIsDeletedFalse();
	List<MethodeTraitementEauEcole> findByIsDeletedTrue();
	List<MethodeTraitementEauEcole> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<MethodeTraitementEauEcole> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
