package ne.mha.sinea.nomenclature.typeAmenagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeAmenagementForm {

	private int code;
	private String libelle;
	
}
