package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class DisponibiliteEauPVCController {

	@Autowired
	DisponibiliteEauPVCRepository disponibiliteEauPVCRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/disponibiliteEauPVC")
	public String  addDisponibiliteEauPVC(DisponibiliteEauPVCForm disponibiliteEauPVCForm, Model model) {
		
		try{
			List<DisponibiliteEauPVC> disponibiliteEauPVCs = disponibiliteEauPVCRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("disponibiliteEauPVCs", disponibiliteEauPVCs);

			model.addAttribute("viewPath", "nomenclature/disponibiliteEauPVC/disponibiliteEauPVC");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
