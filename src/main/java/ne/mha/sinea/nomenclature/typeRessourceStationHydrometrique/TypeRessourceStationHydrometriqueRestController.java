package ne.mha.sinea.nomenclature.typeRessourceStationHydrometrique;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeRessourceStationHydrometriqueRestController {

	@Autowired
	TypeRessourceStationHydrometriqueRepository typeRessourceStationHydrometriqueService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeRessourceStationHydrometrique")
	public int addTypeRessourceStationHydrometrique(@Validated TypeRessourceStationHydrometriqueForm typeRessourceStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		TypeRessourceStationHydrometrique savedTypeRessourceStationHydrometrique = new TypeRessourceStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeRessourceStationHydrometrique P = new TypeRessourceStationHydrometrique();
					P.setLibelle(typeRessourceStationHydrometriqueForm.getLibelle());
					savedTypeRessourceStationHydrometrique = typeRessourceStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRessourceStationHydrometrique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeRessourceStationHydrometrique")
    public int updateTypeRessourceStationHydrometrique(@Validated TypeRessourceStationHydrometriqueForm typeRessourceStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		TypeRessourceStationHydrometrique savedTypeRessourceStationHydrometrique = new TypeRessourceStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeRessourceStationHydrometrique P = typeRessourceStationHydrometriqueService.findByCode(typeRessourceStationHydrometriqueForm.getCode());
					P.setLibelle(typeRessourceStationHydrometriqueForm.getLibelle());
					savedTypeRessourceStationHydrometrique = typeRessourceStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRessourceStationHydrometrique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeRessourceStationHydrometrique")
    public int deleteTypeRessourceStationHydrometrique(@Validated TypeRessourceStationHydrometriqueForm typeRessourceStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		TypeRessourceStationHydrometrique savedTypeRessourceStationHydrometrique = new TypeRessourceStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeRessourceStationHydrometrique P = typeRessourceStationHydrometriqueService.findByCode(typeRessourceStationHydrometriqueForm.getCode());
					P.setIsDeleted(true);
					savedTypeRessourceStationHydrometrique = typeRessourceStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRessourceStationHydrometrique.getCode();
		
        
    }
}
