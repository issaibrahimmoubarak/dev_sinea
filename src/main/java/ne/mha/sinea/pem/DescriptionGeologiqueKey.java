package ne.mha.sinea.pem;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class DescriptionGeologiqueKey implements Serializable {
	@Column(name = "code_pem")
	private Integer codePem;
	@Column(name = "cote_superieure")
	private Double coteSuperieure;
	@Column(name = "cote_inferieure")
	private Double coteInferieure;
}