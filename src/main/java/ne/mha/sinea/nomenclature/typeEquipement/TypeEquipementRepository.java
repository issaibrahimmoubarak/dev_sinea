package ne.mha.sinea.nomenclature.typeEquipement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeEquipementRepository extends CrudRepository<TypeEquipement, Integer> {
	TypeEquipement findByCode(Integer code);
	TypeEquipement findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeEquipement> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeEquipement> findByIsDeletedFalse();
	List<TypeEquipement> findByIsDeletedTrue();
	List<TypeEquipement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeEquipement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
