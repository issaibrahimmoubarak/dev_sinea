package ne.mha.sinea.nomenclature.marquePompe;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class MarquePompeController {

	@Autowired
	MarquePompeRepository marquePompeService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/marquePompe")
	public String  addMarquePompe(MarquePompeForm marquePompeForm, Model model) {
		try{
			List<MarquePompe> marquePompes = marquePompeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("marquePompes", marquePompes);

			model.addAttribute("viewPath", "nomenclature/marquePompe/marquePompe");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
