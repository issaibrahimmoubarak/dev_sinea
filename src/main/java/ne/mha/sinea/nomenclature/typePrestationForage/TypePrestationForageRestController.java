package ne.mha.sinea.nomenclature.typePrestationForage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypePrestationForageRestController {

	@Autowired
	TypePrestationForageRepository typePrestationForageRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typePrestationForage")
	public int addTypePrestationForage(@Validated TypePrestationForageForm typePrestationForageForm,BindingResult bindingResult, Model model) {
		TypePrestationForage savedTypePrestationForage = new TypePrestationForage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestationForage P = new TypePrestationForage();
					P.setLibelle(typePrestationForageForm.getLibelle());
					savedTypePrestationForage = typePrestationForageRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationForage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypePrestationForage")
    public int updateTypePrestationForage(@Validated TypePrestationForageForm typePrestationForageForm,BindingResult bindingResult, Model model) {
		TypePrestationForage savedTypePrestationForage = new TypePrestationForage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestationForage P = typePrestationForageRepository.findByCode(typePrestationForageForm.getCode());
					P.setLibelle(typePrestationForageForm.getLibelle());
					savedTypePrestationForage = typePrestationForageRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationForage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypePrestationForage")
    public int deleteTypePrestationForage(@Validated TypePrestationForageForm typePrestationForageForm,BindingResult bindingResult, Model model) {
		TypePrestationForage savedTypePrestationForage = new TypePrestationForage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestationForage P = typePrestationForageRepository.findByCode(typePrestationForageForm.getCode());
					P.setIsDeleted(true);
					savedTypePrestationForage = typePrestationForageRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationForage.getCode();
		
        
    }
}
