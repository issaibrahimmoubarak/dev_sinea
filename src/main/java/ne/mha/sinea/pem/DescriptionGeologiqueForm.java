package ne.mha.sinea.pem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DescriptionGeologiqueForm {
	//private Integer codePEM;
	private Double coteSuperieure;
	private Double coteInferieure;
	private Integer codeNatureSol;
}
