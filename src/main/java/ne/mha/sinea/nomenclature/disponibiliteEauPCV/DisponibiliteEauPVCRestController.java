package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class DisponibiliteEauPVCRestController {

	@Autowired
	DisponibiliteEauPVCRepository disponibiliteEauPVCRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/disponibiliteEauPVC")
	public int addDisponibiliteEauPVC(@Validated DisponibiliteEauPVCForm disponibiliteEauPVCForm,BindingResult bindingResult, Model model) {
		DisponibiliteEauPVC savedDisponibiliteEauPVC = new DisponibiliteEauPVC();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					DisponibiliteEauPVC P = new DisponibiliteEauPVC();
					P.setLibelle(disponibiliteEauPVCForm.getLibelle());
					savedDisponibiliteEauPVC = disponibiliteEauPVCRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEauPVC.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateDisponibiliteEauPVC")
    public int updateDisponibiliteEauPVC(@Validated DisponibiliteEauPVCForm disponibiliteEauPVCForm,BindingResult bindingResult, Model model) {
		DisponibiliteEauPVC savedDisponibiliteEauPVC = new DisponibiliteEauPVC();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					DisponibiliteEauPVC P = disponibiliteEauPVCRepository.findByCode(disponibiliteEauPVCForm.getCode());
					P.setLibelle(disponibiliteEauPVCForm.getLibelle());
					savedDisponibiliteEauPVC = disponibiliteEauPVCRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEauPVC.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteDisponibiliteEauPVC")
    public int deleteDisponibiliteEauPVC(@Validated DisponibiliteEauPVCForm disponibiliteEauPVCForm,BindingResult bindingResult, Model model) {
		DisponibiliteEauPVC savedDisponibiliteEauPVC = new DisponibiliteEauPVC();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					DisponibiliteEauPVC P = disponibiliteEauPVCRepository.findByCode(disponibiliteEauPVCForm.getCode());
					P.setIsDeleted(true);
					savedDisponibiliteEauPVC = disponibiliteEauPVCRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEauPVC.getCode();
		
        
    }
}
