package ne.mha.sinea.nomenclature.typeReseau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeReseauRepository extends CrudRepository<TypeReseau, Integer> {
	TypeReseau findByCode(Integer code);
	TypeReseau findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeReseau> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeReseau> findByIsDeletedFalse();
	List<TypeReseau> findByIsDeletedTrue();
	List<TypeReseau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeReseau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
