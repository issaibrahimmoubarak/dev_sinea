package ne.mha.sinea.nomenclature.marqueOnduleur;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface MarqueOnduleurRepository extends CrudRepository<MarqueOnduleur, Integer> {
	MarqueOnduleur findByCode(Integer code);
	MarqueOnduleur findByIsDeletedFalseAndLibelle(String libelle);
	List<MarqueOnduleur> findByIsDeletedFalseOrderByLibelleAsc();
	List<MarqueOnduleur> findByIsDeletedFalse();
	List<MarqueOnduleur> findByIsDeletedTrue();
	List<MarqueOnduleur> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<MarqueOnduleur> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
