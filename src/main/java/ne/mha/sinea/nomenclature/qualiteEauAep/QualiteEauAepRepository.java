package ne.mha.sinea.nomenclature.qualiteEauAep;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface QualiteEauAepRepository extends CrudRepository<QualiteEauAep, Integer> {
	QualiteEauAep findByCode(Integer code);
	QualiteEauAep findByIsDeletedFalseAndLibelle(String libelle);
	List<QualiteEauAep> findByIsDeletedFalseOrderByLibelleAsc();
	List<QualiteEauAep> findByIsDeletedFalse();
	List<QualiteEauAep> findByIsDeletedTrue();
	List<QualiteEauAep> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<QualiteEauAep> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
