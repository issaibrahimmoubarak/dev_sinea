package ne.mha.sinea.nomenclature.marqueOnduleur;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class MarqueOnduleurController {

	@Autowired
	MarqueOnduleurRepository marqueOnduleurRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/marqueOnduleur")
	public String  addMarqueOnduleur(MarqueOnduleurForm marqueOnduleurForm, Model model) {
		try{
			List<MarqueOnduleur> marqueOnduleur = marqueOnduleurRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("MarqueOnduleur", marqueOnduleur);

			model.addAttribute("viewPath", "nomenclature/marqueOnduleur/marqueOnduleur");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
