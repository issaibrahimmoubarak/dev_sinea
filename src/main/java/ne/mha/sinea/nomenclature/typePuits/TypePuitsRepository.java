package ne.mha.sinea.nomenclature.typePuits;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypePuitsRepository extends CrudRepository<TypePuits, Integer> {
	TypePuits findByCode(Integer code);
	TypePuits findByIsDeletedFalseAndLibelle(String libelle);
	List<TypePuits> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypePuits> findByIsDeletedFalse();
	List<TypePuits> findByIsDeletedTrue();
	List<TypePuits> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypePuits> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
