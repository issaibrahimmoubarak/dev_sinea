package ne.mha.sinea.nomenclature.typePrestationForage;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypePrestationForageRepository extends CrudRepository<TypePrestationForage, Integer> {
	TypePrestationForage findByCode(Integer code);
	TypePrestationForage findByIsDeletedFalseAndLibelle(String libelle);
	List<TypePrestationForage> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypePrestationForage> findByIsDeletedFalse();
	List<TypePrestationForage> findByIsDeletedTrue();
	List<TypePrestationForage> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypePrestationForage> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
