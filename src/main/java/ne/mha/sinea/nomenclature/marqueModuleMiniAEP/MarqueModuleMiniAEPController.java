package ne.mha.sinea.nomenclature.marqueModuleMiniAEP;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class MarqueModuleMiniAEPController {

	@Autowired
	MarqueModuleMiniAEPRepository marqueModuleMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/marqueModuleMiniAEP")
	public String  addMarqueModuleMiniAEP(MarqueModuleMiniAEPForm marqueModuleMiniAEPForm, Model model) {
		try{
			List<MarqueModuleMiniAEP> marqueModuleMiniAEPs = marqueModuleMiniAEPRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("marqueModuleMiniAEPs", marqueModuleMiniAEPs);

			model.addAttribute("viewPath", "nomenclature/marqueModuleMiniAEP/marqueModuleMiniAEP");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
