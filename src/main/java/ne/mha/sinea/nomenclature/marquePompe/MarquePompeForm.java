package ne.mha.sinea.nomenclature.marquePompe;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarquePompeForm {

	private int code;
	private String libelle;
	
}
