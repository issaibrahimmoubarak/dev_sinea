package ne.mha.sinea.pem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.natureTube.NatureTube;
import ne.mha.sinea.nomenclature.typeTube.TypeTube;

@Data
@Entity
@Table(name = "equipement_forage")
public class EquipementForage extends CommonProperties{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	private Integer code;
	@Column(name = "numero_tube")
	private Integer numeroTube;
	@ManyToOne
	private PEM pem;
	@JoinColumn(name = "code_pem", referencedColumnName = "code")
	@Column(name = "cote_superieure")
	private Double coteSuperieure;
	@Column(name = "cote_inferieure")
	private Double coteInferieure;
	@JoinColumn(name = "code_nature_tube", referencedColumnName = "code")
	@ManyToOne
	private NatureTube natureTube;
	@JoinColumn(name = "code_type_tube", referencedColumnName = "code")
	@ManyToOne
	private TypeTube typeTube;
	@Column(name = "diametre")
	private Double diametre;
}
