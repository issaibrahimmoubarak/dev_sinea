package ne.mha.sinea.saisie.ouvragesAssainissement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.lieu.Lieu;
import ne.mha.sinea.nomenclature.typeLatrines.TypeLatrines;
import ne.mha.sinea.nomenclature.typeOuvrageAssainissement.TypeOuvrageAssainissement;
import ne.mha.sinea.referentiel.etablissementScolaire.EtablissementScolaire;
import ne.mha.sinea.referentiel.formationSanitaire.FormationSanitaire;


//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "OuvragesAssainissement")
public class OuvragesAssainissement extends CommonProperties {

	// annotation pour indiquier l'identifiant d'une table
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "code_ins")
	private String codeINS;
	@Column(name = "indentification")
	private String indentification;
	@Column(name = "nb_dispositif_interieur")
	private int nbDispositifInterieur;
	@Column(name = "nb_dispositif_enceinte")
	private int nbDispositifEnceinte;
	@JoinColumn(name = "code_lieu", referencedColumnName = "code")
	@ManyToOne
	private Lieu lieu;
	@JoinColumn(name = "code_type_latrines", referencedColumnName = "code")
	@ManyToOne
	private TypeLatrines typeLatrines;
	@JoinColumn(name = "code_type_ouvrage_assainissement", referencedColumnName = "code")
	@ManyToOne
	private TypeOuvrageAssainissement typeOuvrageAssainissement;

	// @Column(name = "lieu_ecole")
	// private String lieuEcole;
	// @Column(name = "lieu_menage")
	// private String lieuMenage;
	// @Column(name = "lieu_marches")
	// private String lieuMarches;
	// @Column(name = "lieu_culte")
	// private String lieuCulte;
	// @Column(name = "lieu_gare_routier")
	// private String lieuGareRoutier;
	// @Column(name = "type_latrine_manuelle")
	// private Boolean typeLatrineManuelle;
	// @Column(name = "type_latrine_mecanique")
	// private Boolean typeLatrineMecanique;
	// @Column(name = "type_latrine_eco_san")
	// private Boolean typeLatrineEcoSan;
	// @Column(name = "type_latrine_san_plat")
	// private Boolean typeLatrineSanPlat;
	// @Column(name = "type_latrine_une_fosse")
	// private Boolean typeLatrineUneFosse;
	// @Column(name = "type_latrine_double_fosse")
	// private Boolean typeLatrineDoubleFosse;
	// @Column(name = "type_ouvrage_assai_latrine")
	// private Boolean typeOuvrageAssaiLatrine;
	// @Column(name = "type_ouvrage_assai_bac")
	// private Boolean typeOuvrageAssaiBac;
	// @Column(name = "type_ouvrage_assai_dispo")
	// private Boolean typeOuvrageAssaiDispo;
	// @Column(name = "type_ouvrage_assai_douche")
	// private Boolean typeOuvrageAssaiDouche;
	// @Column(name = "type_ouvrage_assai_uri")
	// private Boolean typeOuvrageAssaiUri;
	// @Column(name = "type_ouvrage_assai_dp")
	// private Boolean typeOuvrageAssaiDp;
	// @Column(name = "type_ouvrage_assai_station")
	// private Boolean typeOuvrageAssaiStation;
	// // @Column(name = "type_ouvrage_assai_autre")
	// private Boolean typeOuvrageAssaiAutre;
	// @Column(name = "type_ouvrage_assai_precision")
	// private Boolean typeOuvrageAssaiPrecision;
	
	
	

}
