package ne.mha.sinea.saisie.ouvragesAssainissement;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface OuvragesAssainissementRepository extends CrudRepository<OuvragesAssainissement, Integer> {
	OuvragesAssainissement findByCode(Integer code);
	OuvragesAssainissement findByCodeINS(String codeINS);
	// List<OuvragesAssainissement> findByEtablissementScolaire_Denomination(String denomination);
	// List<OuvragesAssainissement> findByEtablissementScolaire_Code(Integer code);
	// List<OuvragesAssainissement> findByFormationSanitaire_Denomination(String denomination);
	// List<OuvragesAssainissement> findByFormationSanitaire_Code(Integer code);
	List<OuvragesAssainissement> findByIsDeletedFalse();
	List<OuvragesAssainissement> findByIsDeletedTrue();

}
