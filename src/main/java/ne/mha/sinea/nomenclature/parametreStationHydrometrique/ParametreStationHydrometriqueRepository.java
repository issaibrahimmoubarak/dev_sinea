package ne.mha.sinea.nomenclature.parametreStationHydrometrique;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface ParametreStationHydrometriqueRepository extends CrudRepository<ParametreStationHydrometrique, Integer> {
	ParametreStationHydrometrique findByCode(Integer code);
	ParametreStationHydrometrique findByIsDeletedFalseAndLibelle(String libelle);
	List<ParametreStationHydrometrique> findByIsDeletedFalseOrderByLibelleAsc();
	List<ParametreStationHydrometrique> findByIsDeletedFalse();
	List<ParametreStationHydrometrique> findByIsDeletedTrue();
	List<ParametreStationHydrometrique> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<ParametreStationHydrometrique> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
