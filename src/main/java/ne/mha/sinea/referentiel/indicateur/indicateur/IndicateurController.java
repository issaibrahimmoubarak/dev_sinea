package ne.mha.sinea.referentiel.indicateur.indicateur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;

@Controller
public class IndicateurController {

	@Autowired
	IndicateurExcelService indicateurExcelService;

	@Autowired
	IndicateurRepository indicateurService;

	@Autowired
	TypeClassificationRepository typeClassificationService;

	// @PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/indicateur")
	public String indicateur(IndicateurForm indicateurForm, Model model) {
		try {
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<Indicateur> indicateurs = indicateurService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("indicateurs", indicateurs);
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Indicateur");
			model.addAttribute("viewPath", "referentiel/indicateur/indicateur");

		} catch (Exception e) {

		}
		return Template.appTemplate;
	}

	// @PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/uploadIndicateur/{code}")
	public String uploadIndicateur(@PathVariable("code") int code, IndicateurForm indicateurForm, Model model) {
		try {
			model.addAttribute("typeClassification_code", code);
		} catch (Exception e) {

		}

		return "indicateur/uploadIndicateur";

	}

	// @PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/uploadIndicateur/{code}")
	public RedirectView uploadIndicateurSubmit(@PathVariable("code") int code,
			@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
		String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		final RedirectView redirectView = new RedirectView("/classificationIndicateur/" + code, true);
		// si le fichier a la bonne extension
		if (TYPE.equals(file.getContentType())) {
			try {
				List<String> unsavedIndicateurs = indicateurExcelService.importData(file);
				redirectAttributes.addFlashAttribute("unsavedIndicateurs", unsavedIndicateurs);

			} catch (Exception e) {
				System.out.println(e);
				// indiquer que l'operation d'ajout a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");

			}
		} else {
			// indiquer que l'operation d'ajout a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		// redirection vers la page d'ajout
		return redirectView;

	}

}
