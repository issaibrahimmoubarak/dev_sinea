package ne.mha.sinea.nomenclature.etatPointEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface EtatPointEauRepository extends CrudRepository<EtatPointEau, Integer> {
	EtatPointEau findByCode(Integer code);
	EtatPointEau findByIsDeletedFalseAndLibelle(String libelle);
	List<EtatPointEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<EtatPointEau> findByIsDeletedFalse();
	List<EtatPointEau> findByIsDeletedTrue();
	List<EtatPointEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<EtatPointEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
