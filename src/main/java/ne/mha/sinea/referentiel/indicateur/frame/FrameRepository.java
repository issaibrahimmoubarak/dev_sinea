package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface FrameRepository extends CrudRepository<Frame, Integer>  {

	List<Frame> findByIsDeletedFalse();
	List<Frame> findByIsDeletedTrue();
	List<Frame> findByIsDeletedFalseOrderByLibelleAsc();
	Frame findByCode(Integer code);
	
}
