package ne.mha.sinea.nomenclature.typeLatrines;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeLatrinesRestController {

	@Autowired
	TypeLatrinesRepository typeLatrinesRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeLatrines")
	public int addTypeLatrines(@Validated TypeLatrinesForm typeLatrinesForm,BindingResult bindingResult, Model model) {
		TypeLatrines savedTypeLatrines = new TypeLatrines();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLatrines P = new TypeLatrines();
					P.setLibelle(typeLatrinesForm.getLibelle());
					savedTypeLatrines = typeLatrinesRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLatrines.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeLatrines")
    public int updateTypeLatrines(@Validated TypeLatrinesForm typeLatrinesForm,BindingResult bindingResult, Model model) {
		TypeLatrines savedTypeLatrines = new TypeLatrines();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLatrines P = typeLatrinesRepository.findByCode(typeLatrinesForm.getCode());
					P.setLibelle(typeLatrinesForm.getLibelle());
					savedTypeLatrines = typeLatrinesRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLatrines.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeLatrines")
    public int deleteTypeLatrines(@Validated TypeLatrinesForm typeLatrinesForm,BindingResult bindingResult, Model model) {
		TypeLatrines savedTypeLatrines = new TypeLatrines();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLatrines P = typeLatrinesRepository.findByCode(typeLatrinesForm.getCode());
					P.setIsDeleted(true);
					savedTypeLatrines = typeLatrinesRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLatrines.getCode();
		
        
    }
}
