package ne.mha.sinea.systemeAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrageRepository;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.propriete.ProprieteRepository;
import ne.mha.sinea.nomenclature.typeSystemeAEP.TypeSystemeAEPRepository;
import ne.mha.sinea.referentiel.projet.ProjetRepository;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class SystemeAEPRestController {

	@Autowired
	SystemeAEPRepository systemeAEPService;
	@Autowired
	TypeSystemeAEPRepository typeSystemeAEPService;
	@Autowired
	ProprieteRepository proprieteService;
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	FinancementRepository financementService;
	@Autowired
	ProjetRepository projetService;
	@Autowired
	EtatOuvrageRepository etatOuvrageService;
	
	@PostMapping("/addAEP")
	public int addAEP(@Validated InitAEPForm aep,BindingResult bindingResult) {
		SystemeAEP savedSystemeAEP = new SystemeAEP();
		if (!bindingResult.hasErrors()) {
		//if(true) {	
			try {
					System.out.println("debut...........");
					SystemeAEP saep = new SystemeAEP();
					saep.setNumeroIRH(aep.getNumeroIRH());
					saep.setNomAEP(aep.getNomAEP());
					savedSystemeAEP = systemeAEPService.save(saep);
					System.out.println("fin..........."+savedSystemeAEP.getCode());
					
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
    			System.out.println(e);
				}
    	}
		
		return savedSystemeAEP.getCode();
		
        
    }
	
	@PostMapping("/aepInfosGenerales/{code}")
	public int infosGeneralesSystemeAEP(@PathVariable("code") int code,@Validated SystemeAEPInfosGeneralesForm aep,BindingResult bindingResult) {
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					SystemeAEP saep = systemeAEPService.findByCode(code);
					saep.setNumeroIRH(aep.getNumeroIRH());
					saep.setAnneeRealisation(aep.getAnneeRealisation());
					saep.setTypeSystemeAEP(typeSystemeAEPService.findByCode(aep.getCodeTypeSystemeAEP()));
					saep.setPropriete(proprieteService.findByCode(aep.getCodePropriete()));
					saep.setFinancement(financementService.findByCode(aep.getCodeFinancement()));
					saep.setProjet(projetService.findByCode(aep.getCodeProjet()));
					saep.setLocalite(localiteService.findByCode(aep.getCodeLocalite()));
					saep.setEtatOuvrage(etatOuvrageService.findByCode(aep.getCodeEtatOuvrage()));
					saep.setSourceEnergieSolaire(aep.isSourceEnergieSolaire());
					saep.setSourceEnergieThermique(aep.isSourceEnergieThermique());
					saep.setSourceEnergieReseauElectrique(aep.isSourceEnergieReseauElectrique());
					saep.setSourceEnergieEolienne(aep.isSourceEnergieEolienne());
					saep.setSourceEnergieAutres(aep.isSourceEnergieAutres());
					saep.setPrecisionEnergieAutres(aep.getPrecisionEnergieAutres());

				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		
		return 1;
        
    }
}
