package ne.sinea.piezometre;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.stationMeteo.StationForm;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PiezometreForm {

	private Timestamp createdAt;
	private Date createdBy;
	private boolean isDeleted;
	private Timestamp modifieddAt;
	private Date modifiedBy;
	private Date dateObservation;
	private Double hauteurPiezo;
	private Date heureObservation;
	private Double profondeurPiezo;
	private Double pemCode;
	private boolean isDeletedCentral;
	private boolean isDeletedRegional;

}
