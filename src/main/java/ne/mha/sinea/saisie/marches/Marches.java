package ne.mha.sinea.saisie.marches;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;

import ne.mha.sinea.nomenclature.typeMarches.TypeMarches;
import ne.mha.sinea.nomenclature.modePassation.ModePassation;
import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.intervenant.Intervenant;

@Data
// annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
// annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
// indique qu'il s'agit d'une entité=> création d'une table au niveau de la base
// de données
@Entity
@Table(name = "marche")
public class Marches extends CommonProperties {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "annee_marches_publique")
	private Date anneeMarchesPublique;
	@Column(name = "objet")
	private String objet;
	@Column(name = "cout_previsionnel")
	private double coutPrevisionnel;
	@Column(name = "date_lancement_procedure")
	private Date dateLancementProcedure;
	@Column(name = "date_attribution_contrat")
	private Date dateAttributionContrat;
	@Column(name = "date_demarrage_prestation")
	private Date dateDemarragePrestation;
	@Column(name = "date_achevement_prestation")
	private Date dateAchevementPrestation;

	@JoinColumn(name = "code_intervenant", referencedColumnName = "code")
	@ManyToOne
	private Intervenant intervenant;
	@JoinColumn(name = "code_type_marches", referencedColumnName = "code")
	@ManyToOne
	private TypeMarches typeMarches;
	@JoinColumn(name = "code_financement", referencedColumnName = "code")
	@ManyToOne
	private Financement financement;
	@JoinColumn(name = "code_mode_passation", referencedColumnName = "code")
	@ManyToOne
	private ModePassation modePassation;

}
