package ne.mha.sinea.referentiel.formationSanitaire;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface FormationSanitaireRepository extends CrudRepository<FormationSanitaire, Integer> {
	FormationSanitaire findByCode(Integer code);
	FormationSanitaire findBydenomination(String denomination);
	List<FormationSanitaire> findByIsDeletedFalse();
	List<FormationSanitaire> findByIsDeletedTrue();
}
