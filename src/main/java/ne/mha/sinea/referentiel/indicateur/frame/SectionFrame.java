package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "section_frame")
public class SectionFrame extends CommonProperties {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "libelle")
	private String libelle;
	@Column(name = "sources")
	@ElementCollection(targetClass=Integer.class)
	private List<Integer> sources = new ArrayList<>();
	@Column(name = "indicateurs")
	@ElementCollection(targetClass=Integer.class)
	private List<Integer> indicateurs = new ArrayList<>();
	@Column(name = "zones")
	@ElementCollection(targetClass=Integer.class)
	private List<Integer> zones = new ArrayList<>();
	@Column(name = "periodes")
	@ElementCollection(targetClass=Integer.class)
	private List<Integer> periodes = new ArrayList<>();
}
