package ne.mha.sinea.nomenclature.typeReservoir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeReservoirForm {

	private int code;
	private String libelle;
	
}
