package ne.mha.sinea.nomenclature.marqueModuleMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface MarqueModuleMiniAEPRepository extends CrudRepository<MarqueModuleMiniAEP, Integer> {
	MarqueModuleMiniAEP findByCode(Integer code);
	MarqueModuleMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<MarqueModuleMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<MarqueModuleMiniAEP> findByIsDeletedFalse();
	List<MarqueModuleMiniAEP> findByIsDeletedTrue();
	List<MarqueModuleMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<MarqueModuleMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
