package ne.mha.sinea.nomenclature.propriete;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ProprieteRestController {

	@Autowired
	ProprieteRepository proprieteService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/propriete")
	public int addPropriete(@Validated ProprieteForm proprieteForm,BindingResult bindingResult, Model model) {
		Propriete savedPropriete = new Propriete();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Propriete P = new Propriete();
					P.setLibelle(proprieteForm.getLibelle());
					savedPropriete = proprieteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPropriete.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updatePropriete")
    public int updatePropriete(@Validated ProprieteForm proprieteForm,BindingResult bindingResult, Model model) {
		Propriete savedPropriete = new Propriete();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Propriete P = proprieteService.findByCode(proprieteForm.getCode());
					P.setLibelle(proprieteForm.getLibelle());
					savedPropriete = proprieteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPropriete.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deletePropriete")
    public int deletePropriete(@Validated ProprieteForm proprieteForm,BindingResult bindingResult, Model model) {
		Propriete savedPropriete = new Propriete();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Propriete P = proprieteService.findByCode(proprieteForm.getCode());
					P.setIsDeleted(true);
					savedPropriete = proprieteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPropriete.getCode();
		
        
    }
}
