package ne.mha.sinea.referentiel.indicateur.structure;

import java.util.List;

import org.springframework.data.repository.CrudRepository;



public interface StructureRepository extends CrudRepository<Structure, Integer> {
	Structure findByCode(Integer code);
	Structure findByIsDeletedFalseAndLibelle(String libelle);
	List<Structure> findByIsDeletedFalseOrderByLibelleAsc();
	List<Structure> findByIsDeletedFalse();
	List<Structure> findByIsDeletedTrue();
	

}
