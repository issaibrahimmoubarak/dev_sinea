package ne.mha.sinea.nomenclature.financement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface FinancementRepository extends CrudRepository<Financement, Integer> {
	Financement findByCode(Integer code);
	Financement findByIsDeletedFalseAndLibelle(String libelle);
	List<Financement> findByIsDeletedFalseOrderByLibelleAsc();
	List<Financement> findByIsDeletedFalse();
	List<Financement> findByIsDeletedTrue();
	List<Financement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Financement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
