package ne.mha.sinea.nomenclature.typeEquipementStationMeteo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeEquipementStationMeteoRepository extends CrudRepository<TypeEquipementStationMeteo, Integer> {
	TypeEquipementStationMeteo findByCode(Integer code);
	TypeEquipementStationMeteo findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeEquipementStationMeteo> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeEquipementStationMeteo> findByIsDeletedFalse();
	List<TypeEquipementStationMeteo> findByIsDeletedTrue();
	List<TypeEquipementStationMeteo> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeEquipementStationMeteo> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
