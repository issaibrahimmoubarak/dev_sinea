package ne.mha.sinea.nomenclature.hydraulique;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HydrauliqueForm {

	private int code;
	private String libelle;
	
}
