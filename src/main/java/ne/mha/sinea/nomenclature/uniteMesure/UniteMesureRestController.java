package ne.mha.sinea.nomenclature.uniteMesure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class UniteMesureRestController {

	@Autowired
	UniteMesureRepository uniteMesureRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/uniteMesure")
	public int addUniteMesure(@Validated UniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		UniteMesure savedUniteMesure = new UniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					UniteMesure P = new UniteMesure();
					P.setLibelle(uniteMesureForm.getLibelle());
					savedUniteMesure = uniteMesureRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateUniteMesure")
    public int updateUniteMesure(@Validated UniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		UniteMesure savedUniteMesure = new UniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					UniteMesure P = uniteMesureRepository.findByCode(uniteMesureForm.getCode());
					P.setLibelle(uniteMesureForm.getLibelle());
					savedUniteMesure = uniteMesureRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteUniteMesure")
    public int deleteUniteMesure(@Validated UniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		UniteMesure savedUniteMesure = new UniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					UniteMesure P = uniteMesureRepository.findByCode(uniteMesureForm.getCode());
					P.setIsDeleted(true);
					savedUniteMesure = uniteMesureRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
}
