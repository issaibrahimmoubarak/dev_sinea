package ne.mha.sinea.nomenclature.typeEquipement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeEquipementForm {

	private int code;
	private String libelle;
	
}
