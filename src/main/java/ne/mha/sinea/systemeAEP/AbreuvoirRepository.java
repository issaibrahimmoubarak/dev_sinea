package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface AbreuvoirRepository extends CrudRepository<Abreuvoir, Integer> {
	
	Abreuvoir findByCode(Integer code);
	List<Abreuvoir> findBySystemeAEP_NumeroIRH(String IRH);
	
}