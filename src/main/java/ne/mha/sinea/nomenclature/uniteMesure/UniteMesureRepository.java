package ne.mha.sinea.nomenclature.uniteMesure;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface UniteMesureRepository extends CrudRepository<UniteMesure, Integer> {
	UniteMesure findByCode(Integer code);
	UniteMesure findByIsDeletedFalseAndLibelle(String libelle);
	List<UniteMesure> findByIsDeletedFalseOrderByLibelleAsc();
	List<UniteMesure> findByIsDeletedFalse();
	List<UniteMesure> findByIsDeletedTrue();
	List<UniteMesure> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<UniteMesure> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
