package ne.mha.sinea.referentiel.indicateur.sousGroupe;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrame;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrameRepository;

@Controller
public class SousGroupeController {
	@Autowired
	SousGroupeRepository sousGroupeService;
	@Autowired
	TypeClassificationRepository typeClassificationService;
	@Autowired
	SectionFrameRepository sectionFrameService;
	//@PreAuthorize("hasAuthority('gestion des sous groupes')")
	@GetMapping("/sousGroupe")
	public String  addSousGroupe(SousGroupeForm sousGroupeForm, Model model) {
		try{
			List<SousGroupe> sousGroupes = sousGroupeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sousGroupes", sousGroupes);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Sous groupe");
			model.addAttribute("viewPath", "referentiel/sousGroupe/sousGroupe");
			
		}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	

	
}
