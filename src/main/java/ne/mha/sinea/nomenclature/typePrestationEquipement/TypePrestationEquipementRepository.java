package ne.mha.sinea.nomenclature.typePrestationEquipement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypePrestationEquipementRepository extends CrudRepository<TypePrestationEquipement, Integer> {
	TypePrestationEquipement findByCode(Integer code);
	TypePrestationEquipement findByIsDeletedFalseAndLibelle(String libelle);
	List<TypePrestationEquipement> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypePrestationEquipement> findByIsDeletedFalse();
	List<TypePrestationEquipement> findByIsDeletedTrue();
	List<TypePrestationEquipement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypePrestationEquipement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
