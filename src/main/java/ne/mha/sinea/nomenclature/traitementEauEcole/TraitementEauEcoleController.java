package ne.mha.sinea.nomenclature.traitementEauEcole;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TraitementEauEcoleController {

	@Autowired
	TraitementEauEcoleRepository traitementEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/traitementEauEcole")
	public String  addTraitementEauEcole(TraitementEauEcoleForm traitementEauEcoleForm, Model model) {
		try{
			List<TraitementEauEcole> traitementEauEcole = traitementEauEcoleRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("traitementEauEcoles", traitementEauEcole);

			model.addAttribute("viewPath", "nomenclature/traitementEauEcole/traitementEauEcole");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
