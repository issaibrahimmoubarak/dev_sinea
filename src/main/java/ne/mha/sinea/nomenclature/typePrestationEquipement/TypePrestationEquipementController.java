package ne.mha.sinea.nomenclature.typePrestationEquipement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TypePrestationEquipementController {

	@Autowired
	TypePrestationEquipementRepository typePrestationEquipementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typePrestationEquipement")
	public String  addTypePrestationEquipement(TypePrestationEquipementForm typePrestationEquipementForm, Model model) {
		try{
			List<TypePrestationEquipement> typePrestationEquipement = typePrestationEquipementRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typePrestationEquipement", typePrestationEquipement);
			
			model.addAttribute("viewPath", "nomenclature/typePrestationEquipement/typePrestationEquipement");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
