package ne.mha.sinea.referentiel.indicateur.uniteIndicateur;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.unite.Unite;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "unite_indicateur")
public class UniteIndicateur extends CommonProperties {
	
	@EmbeddedId
	private UniteIndicateurKey code;
	 
	@MapsId("codeIndicateur")
	@JoinColumn(name = "code_indicateur", referencedColumnName = "code")
	@ManyToOne
	private Indicateur indicateur;
	
	@MapsId("codeUnite")
	@JoinColumn(name = "code_unite", referencedColumnName = "code")
	@ManyToOne
	private Unite unite;

	
	
}
