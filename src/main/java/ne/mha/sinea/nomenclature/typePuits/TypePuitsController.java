package ne.mha.sinea.nomenclature.typePuits;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TypePuitsController {

	@Autowired
	TypePuitsRepository typePuitsService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typePuits")
	public String  addTypePuits(TypePuitsForm typePuitsForm, Model model) {
		try{
			List<TypePuits> typePuitss = typePuitsService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typePuitss", typePuitss);

			model.addAttribute("viewPath", "nomenclature/typePuits/typePuits");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
