package ne.mha.sinea.nomenclature.typeMarches;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TypeMarchesRepository extends CrudRepository<TypeMarches, Integer> {
	TypeMarches findByCode(Integer code);
	TypeMarches findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeMarches> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeMarches> findByIsDeletedFalse();
	List<TypeMarches> findByIsDeletedTrue();
	List<TypeMarches> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeMarches> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
