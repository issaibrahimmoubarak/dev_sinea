package ne.mha.sinea.nomenclature.typeMarches;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ne.mha.sinea.Template;

@Controller
public class TypeMarchesController {
	@Autowired
	TypeMarchesRepository typeMarchesService;
	
	//@PreAuthorize("hasAuthority('gestion des typeMarchess')")
	@GetMapping("/typeMarches")
	public String  addTypeMarches(TypeMarchesForm typeMarchesForm, Model model) {
		try{
			List<TypeMarches> typeMarchess = typeMarchesService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeMarchess", typeMarchess);
	
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "nomenclature/typeMarches");
			model.addAttribute("viewPath", "nomenclature/typeMarches/typeMarches");
			
		}
		catch(Exception e){
				
		}
		
		return Template.nomenclatureTemplate;

	}
	
		
}
