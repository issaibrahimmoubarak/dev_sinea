package ne.mha.sinea.nomenclature.typeEquipement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TypeEquipementController {

	@Autowired
	TypeEquipementRepository typeEquipementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeEquipement")
	public String  addTypeEquipement(TypeEquipementForm typeEquipementForm, Model model) {
		try{
			List<TypeEquipement> typeEquipement = typeEquipementRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeEquipements", typeEquipement);

			model.addAttribute("viewPath", "nomenclature/typeEquipement/typeEquipement");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
