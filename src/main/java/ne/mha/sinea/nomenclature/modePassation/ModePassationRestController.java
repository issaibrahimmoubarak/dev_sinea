package ne.mha.sinea.nomenclature.modePassation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ModePassationRestController {

	@Autowired
	ModePassationRepository modePassationService;
	//@PreAuthorize("hasAuthority('gestion des modePassations')")
	@PostMapping("/modePassation")
	public int addModePassation(@Validated ModePassationForm modePassationForm,BindingResult bindingResult, Model model) {
		ModePassation savedModePassation = new ModePassation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ModePassation P = new ModePassation();
					P.setLibelle(modePassationForm.getLibelle());
					savedModePassation = modePassationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedModePassation.getCode();
		
        
    }
	@PreAuthorize("hasAuthority('gestion des modePassations')")
	@PostMapping("/updateModePassation")
    public int updateModePassation(@Validated ModePassationForm modePassationForm,BindingResult bindingResult, Model model) {
		ModePassation savedModePassation = new ModePassation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ModePassation P = modePassationService.findByCode(modePassationForm.getCode());
					P.setLibelle(modePassationForm.getLibelle());
					savedModePassation = modePassationService.save(P);
					
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedModePassation.getCode();
		
        
    }
	@PreAuthorize("hasAuthority('gestion des modePassations')")
	@PostMapping("/deleteModePassation")
    public int deleteModePassation(@Validated ModePassationForm modePassationForm,BindingResult bindingResult, Model model) {
		ModePassation savedModePassation = new ModePassation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ModePassation P = modePassationService.findByCode(modePassationForm.getCode());
					P.setIsDeleted(true);
					savedModePassation = modePassationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedModePassation.getCode();
		
        
    }
}
