package ne.mha.sinea.permission;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PrivilegeRepository extends CrudRepository<Privilege, Integer> {

	Privilege findByCode(Integer code);
	Privilege findByLibelle(String libelle);
	Privilege findByIsDeletedFalseAndLibelle(String libelle);
	List<Privilege> findByIsDeletedFalseOrderByLibelleAsc();
	List<Privilege> findByIsDeletedFalse();
	List<Privilege> findByIsDeletedTrue();
	
	List<Privilege> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
}
