package ne.mha.sinea.nomenclature.typeOuvrageAssainissement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeOuvrageAssainissementRepository extends CrudRepository<TypeOuvrageAssainissement, Integer> {
	TypeOuvrageAssainissement findByCode(Integer code);
	TypeOuvrageAssainissement findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeOuvrageAssainissement> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeOuvrageAssainissement> findByIsDeletedFalse();
	List<TypeOuvrageAssainissement> findByIsDeletedTrue();
	List<TypeOuvrageAssainissement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeOuvrageAssainissement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
