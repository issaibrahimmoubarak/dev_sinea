package ne.mha.sinea.referentiel.indicateur.sousGroupeIndicateur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupe;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupeForm;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupeRepository;

@Controller
public class SousGroupeIndicateurController {
@Autowired
	IndicateurRepository indicateurService;
	@Autowired
	SousGroupeRepository sousGroupeService;
	
	@Autowired
	SousGroupeIndicateurRepository sousGroupeIndicateuirService;
	
	@Autowired
	TypeClassificationRepository typeClassificationService;
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/ajouterSousGroupeIndicateur/{code}")
	public String ajouterSousGroupesIndicateur(@PathVariable("code") int code,SousGroupeForm sousGroupeForm, Model model) {
		try{
			Indicateur indicateur = indicateurService.findByCode(code);
			model.addAttribute("indicateur", indicateur);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<SousGroupeIndicateur> sousGroupesIndicateur = sousGroupeIndicateuirService.findByIsDeletedFalseAndIndicateur_Code(code);
			model.addAttribute("sousGroupesIndicateur", sousGroupesIndicateur);
			
			List<SousGroupe> sousGroupes = sousGroupeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sousGroupes", sousGroupes);
			
		}
		catch(Exception e){
				
			}
		return "sousGroupe/ajouterSousGroupeIndicateur";
	}
		
	
}
