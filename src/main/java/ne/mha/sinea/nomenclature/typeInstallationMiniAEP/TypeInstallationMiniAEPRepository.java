package ne.mha.sinea.nomenclature.typeInstallationMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeInstallationMiniAEPRepository extends CrudRepository<TypeInstallationMiniAEP, Integer> {
	TypeInstallationMiniAEP findByCode(Integer code);
	TypeInstallationMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeInstallationMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeInstallationMiniAEP> findByIsDeletedFalse();
	List<TypeInstallationMiniAEP> findByIsDeletedTrue();
	List<TypeInstallationMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeInstallationMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
