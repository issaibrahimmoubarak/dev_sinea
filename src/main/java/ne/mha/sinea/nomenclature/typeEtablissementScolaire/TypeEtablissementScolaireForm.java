package ne.mha.sinea.nomenclature.typeEtablissementScolaire;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeEtablissementScolaireForm {

	private int code;
	private String libelle;
	
}
