package ne.sinea.piezometre;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sun.istack.NotNull;

import ne.mha.sinea.nomenclature.milieu.Milieu;
import ne.mha.sinea.pem.Forage;

public class Piezometre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "created_at")
	private Timestamp createdAt;
	@Column(name = "created_by")
	private Date createdBy;
	@Column(name ="is_deleted")
	private boolean isDeleted;
	@Column(name = "modified_at")
	private Timestamp modifieddAt;
	@Column(name = "modified_by")
	private Date modifiedBy;
	
	@Column(name = "date_observation")
	private Date dateObservation;
	
	@Column(name = "hauteur_piezo")
	private Double hauteurPiezo;
	
	@Column(name = "heure_observation")
	private Date heureObservation;
	
	@Column(name = "profondeur_piezo")
	private Double profondeurPiezo;
	
	@Column(name = "pem_code")
	private Double pemCode;
	
	@Column(name = "is_deleted_central")
	private boolean isDeletedCentral;
	
	
	@Column(name = "is_deleted_regional")
	private boolean isDeletedRegional;
	
	
	 @JoinColumn(name = "code_forage", referencedColumnName = "code")
		@ManyToOne
		private Forage forage;
}
