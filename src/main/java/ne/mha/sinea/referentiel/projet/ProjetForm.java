package ne.mha.sinea.referentiel.projet;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjetForm {

	private int code;
	private String libelle;
	private String zoneIntervention;
	private Date dateDebut;
	private Date dateFin;
	private String financement;
	protected Double montant;
	private String maitreOuvrage;
	private String maitreOeuvre;
	
}
