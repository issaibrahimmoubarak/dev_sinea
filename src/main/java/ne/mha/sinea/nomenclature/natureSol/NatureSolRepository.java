package ne.mha.sinea.nomenclature.natureSol;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface NatureSolRepository extends CrudRepository<NatureSol, Integer> {
	NatureSol findByCode(Integer code);
	NatureSol findByIsDeletedFalseAndLibelle(String libelle);
	List<NatureSol> findByIsDeletedFalseOrderByLibelleAsc();
	List<NatureSol> findByIsDeletedFalse();
	List<NatureSol> findByIsDeletedTrue();
	List<NatureSol> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<NatureSol> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
