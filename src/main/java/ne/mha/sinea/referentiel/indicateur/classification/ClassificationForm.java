package ne.mha.sinea.referentiel.indicateur.classification;
/*
 * classe qui va permettre de valider le formulaire de saisie
 * classification
 */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassificationForm {

	private int code;
	private String libelle;
	private int parent_code;
	private Integer typeClassification_code;
	
}
