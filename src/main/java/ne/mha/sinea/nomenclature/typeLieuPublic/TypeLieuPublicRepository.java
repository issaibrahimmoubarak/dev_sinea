package ne.mha.sinea.nomenclature.typeLieuPublic;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeLieuPublicRepository extends CrudRepository<TypeLieuPublic, Integer> {
	TypeLieuPublic findByCode(Integer code);
	TypeLieuPublic findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeLieuPublic> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeLieuPublic> findByIsDeletedFalse();
	List<TypeLieuPublic> findByIsDeletedTrue();
	List<TypeLieuPublic> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeLieuPublic> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
