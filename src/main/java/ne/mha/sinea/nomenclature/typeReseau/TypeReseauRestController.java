package ne.mha.sinea.nomenclature.typeReseau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeReseauRestController {

	@Autowired
	TypeReseauRepository typeReseauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeReseau")
	public int addTypeReseau(@Validated TypeReseauForm typeReseauForm,BindingResult bindingResult, Model model) {
		TypeReseau savedTypeReseau = new TypeReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeReseau P = new TypeReseau();
					P.setLibelle(typeReseauForm.getLibelle());
					savedTypeReseau = typeReseauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReseau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeReseau")
    public int updateTypeReseau(@Validated TypeReseauForm typeReseauForm,BindingResult bindingResult, Model model) {
		TypeReseau savedTypeReseau = new TypeReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeReseau P = typeReseauRepository.findByCode(typeReseauForm.getCode());
					P.setLibelle(typeReseauForm.getLibelle());
					savedTypeReseau = typeReseauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReseau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeReseau")
    public int deleteTypeReseau(@Validated TypeReseauForm typeReseauForm,BindingResult bindingResult, Model model) {
		TypeReseau savedTypeReseau = new TypeReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeReseau P = typeReseauRepository.findByCode(typeReseauForm.getCode());
					P.setIsDeleted(true);
					savedTypeReseau = typeReseauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReseau.getCode();
		
        
    }
}
