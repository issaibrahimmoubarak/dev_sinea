package ne.mha.sinea.referentiel.indicateur.indicateur;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;

public interface IndicateurRepository extends CrudRepository<Indicateur, Integer> {
	Indicateur findByCode(Integer code);
	Indicateur findByIsDeletedFalseAndLibelle(String libelle);
	List<Indicateur> findByIsDeletedFalseOrderByLibelleAsc();
	List<Indicateur> findByIsDeletedFalse();
	List<Indicateur> findByIsDeletedTrue();
	List<Indicateur> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Indicateur> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	@Transactional 
	@Query(nativeQuery = true, value="select code from indicateur where is_deleted=false and code in (select code_indicateur from indicateur_objectif where is_deleted=false and code_objectif = :code_objectif)")
	List<Integer> findIndicateursUtilises(@Param("code_objectif")Integer code_objectif);
	
	@Transactional 
	@Query(nativeQuery = true, value="(select libelle from indicateur where is_deleted = false)")
	List<String> findListeIndicateur();
	
	
	

}
