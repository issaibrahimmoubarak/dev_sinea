package ne.mha.sinea.nomenclature.typeInstallationMiniAEP;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeInstallationMiniAEPForm {

	private int code;
	private String libelle;
	
}
