package ne.mha.sinea.nomenclature.usageSourceEauEcole;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsageSourceEauEcoleForm {

	private int code;
	private String libelle;
	
}
