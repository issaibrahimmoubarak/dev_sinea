package ne.mha.sinea.nomenclature.etatPointEau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EtatPointEauForm {

	private int code;
	private String libelle;
	
}
