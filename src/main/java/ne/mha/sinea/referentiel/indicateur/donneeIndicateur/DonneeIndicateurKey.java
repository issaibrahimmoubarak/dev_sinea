package ne.mha.sinea.referentiel.indicateur.donneeIndicateur;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class DonneeIndicateurKey implements Serializable  {

	Integer codeIndicateur;
	Integer codeSource;
	Integer codePeriode;
	Integer codeZone;
	Integer codeSousGroupe;
	Integer codeUnite;
}
