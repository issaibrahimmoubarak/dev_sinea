package ne.mha.sinea.nomenclature.typeReseau;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TypeReseauController {

	@Autowired
	TypeReseauRepository typeReseauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeReseau")
	public String  addTypeReseau(TypeReseauForm typeReseauForm, Model model) {
		try{
			List<TypeReseau> typeReseau = typeReseauRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeReseau", typeReseau);

			model.addAttribute("viewPath", "nomenclature/typeReseau/typeReseau");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
