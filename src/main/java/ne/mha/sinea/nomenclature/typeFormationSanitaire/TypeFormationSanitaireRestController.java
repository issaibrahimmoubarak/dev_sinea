package ne.mha.sinea.nomenclature.typeFormationSanitaire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeFormationSanitaireRestController {

	@Autowired
	TypeFormationSanitaireRepository typeFormationSanitaireService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeFormationSanitaire")
	public int addTypeFormationSanitaire(@Validated TypeFormationSanitaireForm typeFormationSanitaireForm,BindingResult bindingResult, Model model) {
		TypeFormationSanitaire savedTypeFormationSanitaire = new TypeFormationSanitaire();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeFormationSanitaire P = new TypeFormationSanitaire();
					P.setLibelle(typeFormationSanitaireForm.getLibelle());
					savedTypeFormationSanitaire = typeFormationSanitaireService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeFormationSanitaire.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeFormationSanitaire")
    public int updateTypeFormationSanitaire(@Validated TypeFormationSanitaireForm typeFormationSanitaireForm,BindingResult bindingResult, Model model) {
		TypeFormationSanitaire savedTypeFormationSanitaire = new TypeFormationSanitaire();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeFormationSanitaire P = typeFormationSanitaireService.findByCode(typeFormationSanitaireForm.getCode());
					P.setLibelle(typeFormationSanitaireForm.getLibelle());
					savedTypeFormationSanitaire = typeFormationSanitaireService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeFormationSanitaire.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeFormationSanitaire")
    public int deleteTypeFormationSanitaire(@Validated TypeFormationSanitaireForm typeFormationSanitaireForm,BindingResult bindingResult, Model model) {
		TypeFormationSanitaire savedTypeFormationSanitaire = new TypeFormationSanitaire();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeFormationSanitaire P = typeFormationSanitaireService.findByCode(typeFormationSanitaireForm.getCode());
					P.setIsDeleted(true);
					savedTypeFormationSanitaire = typeFormationSanitaireService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeFormationSanitaire.getCode();
		
        
    }
}
