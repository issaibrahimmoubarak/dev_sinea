package ne.mha.sinea.nomenclature.typeReservoir;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeReservoirRestController {

	@Autowired
	TypeReservoirRepository typeReservoirRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeReservoir")
	public int addTypeReservoir(@Validated TypeReservoirForm typeReservoirForm,BindingResult bindingResult, Model model) {
		TypeReservoir savedTypeReservoir = new TypeReservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeReservoir P = new TypeReservoir();
					P.setLibelle(typeReservoirForm.getLibelle());
					savedTypeReservoir = typeReservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReservoir.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeReservoir")
    public int updateTypeReservoir(@Validated TypeReservoirForm typeReservoirForm,BindingResult bindingResult, Model model) {
		TypeReservoir savedTypeReservoir = new TypeReservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeReservoir P = typeReservoirRepository.findByCode(typeReservoirForm.getCode());
					P.setLibelle(typeReservoirForm.getLibelle());
					savedTypeReservoir = typeReservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReservoir.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeReservoir")
    public int deleteTypeReservoir(@Validated TypeReservoirForm typeReservoirForm,BindingResult bindingResult, Model model) {
		TypeReservoir savedTypeReservoir = new TypeReservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeReservoir P = typeReservoirRepository.findByCode(typeReservoirForm.getCode());
					P.setIsDeleted(true);
					savedTypeReservoir = typeReservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReservoir.getCode();
		
        
    }
}
