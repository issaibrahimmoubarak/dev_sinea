package ne.mha.sinea.saisie.marches;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;

import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import ne.mha.sinea.Template;

import ne.mha.sinea.nomenclature.modePassation.ModePassation;
import ne.mha.sinea.nomenclature.modePassation.ModePassationRepository;
import ne.mha.sinea.nomenclature.typeMarches.TypeMarches;
import ne.mha.sinea.nomenclature.typeMarches.TypeMarchesRepository;
import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.intervenant.Intervenant;
import ne.mha.sinea.nomenclature.intervenant.IntervenantRepository;

@Controller
public class MarchesController {
	@Autowired
	EntityManager em;

	@Autowired
	MarchesRepository marchesService;
	@Autowired
	IntervenantRepository intervenantService;
	@Autowired
	ModePassationRepository modePassationService;
	@Autowired
	FinancementRepository financementService;
	@Autowired
	TypeMarchesRepository typeMarchesService;

	// formulaire intervenant dont l'ajout , l'import et l'export
	// @PreAuthorize("hasAuthority('Ajout Marches'))
	@GetMapping("/marches")
	public String addMarches(MarchesForm marchesForm, Model model) {
		try {
			List<TypeMarches> typeMarches = typeMarchesService.findByIsDeletedFalse();
			model.addAttribute("typeMarches", typeMarches);
			List<Financement> sourceFinancements = financementService.findByIsDeletedFalse();
			model.addAttribute("sourceFinancements", sourceFinancements);
			List<ModePassation> modePassations = modePassationService.findByIsDeletedFalse();
			model.addAttribute("modePassations", modePassations);
			List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenants", intervenants);
			List<Marches> marche = marchesService.findByIsDeletedFalse();
			model.addAttribute("marche", marche);
			// model.addAttribute("horizontalMenu", "horizontalMenu");
			// model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			// model.addAttribute("breadcrumb", "breadcrumb");

			model.addAttribute("viewPath", "saisie/marches/marches");

		} catch (Exception e) {

		}
		return Template.secondTemplate;
	}

	// validation du formulaire d'ajout intervenant
	@PostMapping("/marches")
	public String addMarchesSubmit(@Validated MarchesForm marchesForm, BindingResult bindingResult,
			Model model) {
		// s'il n'ya pas d'erreur lors de la validation des données du formulaire
		// Marches savedMarches = new Marches();
		if (!bindingResult.hasErrors()) {

			try {
				// récuperer les données saisies
				// System.out.println("ok");
				Marches M = new Marches();
				M.setAnneeMarchesPublique(marchesForm.getAnneeMarchesPublique());
				M.setObjet(marchesForm.getObjet());
				M.setCoutPrevisionnel(marchesForm.getCoutPrevisionnel());
				M.setDateLancementProcedure(marchesForm.getDateLancementProcedure());
				M.setDateAttributionContrat(marchesForm.getDateAttributionContrat());
				M.setDateDemarragePrestation(marchesForm.getDateDemarragePrestation());
				M.setDateAchevementPrestation(marchesForm.getDateAchevementPrestation());
				M.setIntervenant(intervenantService.findByCode(marchesForm.getCodeIntervenant()));
				M.setModePassation(modePassationService.findByCode(marchesForm.getCodeModePassation()));
				M.setFinancement(financementService.findByCode(marchesForm.getCodeFinancement()));
				M.setTypeMarches(typeMarchesService.findByCode(marchesForm.getCodeTypeMarches()));

				marchesService.save(M);

				// indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				List<TypeMarches> typeMarches = typeMarchesService.findByIsDeletedFalse();
				model.addAttribute("typeMarches", typeMarches);
				List<Financement> sourceFinancements = financementService.findByIsDeletedFalse();
				model.addAttribute("sourceFinancements", sourceFinancements);
				List<ModePassation> modePassations = modePassationService.findByIsDeletedFalse();
				model.addAttribute("modePassations", modePassations);
				List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenants", intervenants);
				List<Marches> marche = marchesService.findByIsDeletedFalse();
				model.addAttribute("marche", marche);

				// model.addAttribute("horizontalMenu", "horizontalMenu");
				// model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				// model.addAttribute("breadcrumb", "breadcrumb");

				model.addAttribute("viewPath", "saisie/marches/marches");

			} catch (Exception e) {
				// model.addAttribute("operationStatus", "operationStatus/unsuccess");
				// List<TypeMarches> typeMarches = (List<TypeMarches>)
				// typeMarchesService.findByIsDeletedFalse();
				// model.addAttribute("typeMarches", typeMarches);
				// List<Financement> sourceFinancements = (List<Financement>)
				// financementService.findByIsDeletedFalse();
				// model.addAttribute("sourceFinancements", sourceFinancements);
				// List<ModePassation> modePassations = (List<ModePassation>)
				// modePassationService.findByIsDeletedFalse();
				// model.addAttribute("modePassations", modePassations);
				// List<Intervenant> intervenants = (List<Intervenant>)
				// intervenantService.findByIsDeletedFalse();
				// model.addAttribute("intervenants", intervenants);
				// List<Marches> marches = (List<Marches>)
				// marchesService.findByIsDeletedFalse();
				// model.addAttribute("marches", marches);
				// model.addAttribute("viewPath", "saisie/marches/marches");

			}
		} else {
			try {
				// // System.out.println("erreur");
				// model.addAttribute("operationStatus", "operationStatus/unsuccess");
				// List<TypeMarches> typeMarches = (List<TypeMarches>)
				// typeMarchesService.findByIsDeletedFalse();
				// model.addAttribute("typeMarches", typeMarches);
				// List<Financement> sourceFinancements = (List<Financement>)
				// financementService.findByIsDeletedFalse();
				// model.addAttribute("sourceFinancements", sourceFinancements);
				// List<ModePassation> modePassations = (List<ModePassation>)
				// modePassationService.findByIsDeletedFalse();
				// model.addAttribute("modePassations", modePassations);
				// List<Intervenant> intervenants = (List<Intervenant>)
				// intervenantService.findByIsDeletedFalse();
				// model.addAttribute("intervenants", intervenants);
				// List<Marches> marches = (List<Marches>)
				// marchesService.findByIsDeletedFalse();
				// model.addAttribute("marches", marches);
				// model.addAttribute("viewPath", "saisie/marches/marches");

			} catch (Exception e) {

			}
		}

		return Template.secondTemplate;

	}

	// modification d'une marches
	@GetMapping("/updateMarches/{code}")
	public String updateMarches(@PathVariable("code") int code,
			MarchesForm marchesForm, Model model) {
		try {
			// récuperer les informations du marches à modifier
			Marches marches = marchesService.findByCode(code);
			// envoie des informations du marches à modifier à travers le modele
			marchesForm.setAnneeMarchesPublique(marches.getAnneeMarchesPublique());
			marchesForm.setObjet(marches.getObjet());
			marchesForm.setCoutPrevisionnel(marches.getCoutPrevisionnel());
			marchesForm.setDateLancementProcedure(marches.getDateLancementProcedure());
			marchesForm.setDateAttributionContrat(marches.getDateAttributionContrat());
			marchesForm.setDateDemarragePrestation(marches.getDateDemarragePrestation());
			marchesForm.setDateAchevementPrestation(marches.getDateAchevementPrestation());
			marchesForm.setCodeIntervenant(marches.getIntervenant().getCode());
			marchesForm.setCodeModePassation(marches.getModePassation().getCode());
			marchesForm.setCodeFinancement(marches.getFinancement().getCode());
			marchesForm.setCodeTypeMarches(marches.getTypeMarches().getCode());
			// intervenantForm.setCodeTypeIntervenant(intervenant.getTypeIntervenant().getCode());

			// récuperation de la liste des marchés de la base de données

			List<TypeMarches> typeMarches = typeMarchesService.findByIsDeletedFalse();
			model.addAttribute("typeMarches", typeMarches);
			List<Financement> sourceFinancements = financementService.findByIsDeletedFalse();
			model.addAttribute("sourceFinancements", sourceFinancements);
			List<ModePassation> modePassations = modePassationService.findByIsDeletedFalse();
			model.addAttribute("modePassations", modePassations);
			List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenants", intervenants);
			List<Marches> marche = marchesService.findByIsDeletedFalse();
			model.addAttribute("marche", marche);

			model.addAttribute("viewPath", "saisie/marches/updateMarches");

		} catch (Exception e) {

		}
		return Template.secondTemplate;

	}

	@PostMapping("/updateMarches/{code}")
	public RedirectView updateMarchesSubmit(@Validated MarchesForm marchesForm, BindingResult bindingResult,
			@PathVariable("code") int code, Model model, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/marches", true);
		if (!bindingResult.hasErrors()) {
			try {
				// recupérer les informations saisies
				Marches M = marchesService.findByCode(code);
				M.setAnneeMarchesPublique(marchesForm.getAnneeMarchesPublique());
				M.setObjet(marchesForm.getObjet());
				M.setCoutPrevisionnel(marchesForm.getCoutPrevisionnel());
				M.setDateLancementProcedure(marchesForm.getDateLancementProcedure());
				M.setDateAttributionContrat(marchesForm.getDateAttributionContrat());
				M.setDateDemarragePrestation(marchesForm.getDateDemarragePrestation());
				M.setDateAchevementPrestation(marchesForm.getDateAchevementPrestation());
				M.setIntervenant(intervenantService.findByCode(marchesForm.getCodeIntervenant()));
				M.setModePassation(modePassationService.findByCode(marchesForm.getCodeModePassation()));
				M.setFinancement(financementService.findByCode(marchesForm.getCodeFinancement()));
				M.setTypeMarches(typeMarchesService.findByCode(marchesForm.getCodeTypeMarches()));

				// enregistrer les informations dans la base de données
				marchesService.save(M);

				// indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");

			} catch (Exception e) {
				// indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
		} else {

		}

		// redirection vers la page d'ajout
		return redirectView;

	}

	@GetMapping("/deleteMarches/{code}")
	public RedirectView deleteMarches(@PathVariable("code") int code, Model model,
			RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/marches", true);
		try {
			Marches M = marchesService.findByCode(code);
			M.setIsDeleted(true);
			marchesService.save(M);
			;

			// indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		} catch (Exception e) {
			// indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		// redirection vers la page d'ajout
		return redirectView;

	}

}
