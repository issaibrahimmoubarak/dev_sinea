package ne.mha.sinea.nomenclature.etatCompteurForageMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class EtatCompteurForageMiniAEPRestController {

	@Autowired
	EtatCompteurForageMiniAEPRepository etatCompteurForageMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/etatCompteurForageMiniAEP")
	public int addEtatCompteurForageMiniAEP(@Validated EtatCompteurForageMiniAEPForm etatCompteurForageMiniAEPForm,BindingResult bindingResult, Model model) {
		EtatCompteurForageMiniAEP savedEtatCompteurForageMiniAEP = new EtatCompteurForageMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatCompteurForageMiniAEP P = new EtatCompteurForageMiniAEP();
					P.setLibelle(etatCompteurForageMiniAEPForm.getLibelle());
					savedEtatCompteurForageMiniAEP = etatCompteurForageMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatCompteurForageMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateEtatCompteurForageMiniAEP")
    public int updateEtatCompteurForageMiniAEP(@Validated EtatCompteurForageMiniAEPForm etatCompteurForageMiniAEPForm,BindingResult bindingResult, Model model) {
		EtatCompteurForageMiniAEP savedEtatCompteurForageMiniAEP = new EtatCompteurForageMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatCompteurForageMiniAEP P = etatCompteurForageMiniAEPRepository.findByCode(etatCompteurForageMiniAEPForm.getCode());
					P.setLibelle(etatCompteurForageMiniAEPForm.getLibelle());
					savedEtatCompteurForageMiniAEP = etatCompteurForageMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatCompteurForageMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteEtatCompteurForageMiniAEP")
    public int deleteEtatCompteurForageMiniAEP(@Validated EtatCompteurForageMiniAEPForm etatCompteurForageMiniAEPForm,BindingResult bindingResult, Model model) {
		EtatCompteurForageMiniAEP savedEtatCompteurForageMiniAEP = new EtatCompteurForageMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatCompteurForageMiniAEP P = etatCompteurForageMiniAEPRepository.findByCode(etatCompteurForageMiniAEPForm.getCode());
					P.setIsDeleted(true);
					savedEtatCompteurForageMiniAEP = etatCompteurForageMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatCompteurForageMiniAEP.getCode();
		
        
    }
}
