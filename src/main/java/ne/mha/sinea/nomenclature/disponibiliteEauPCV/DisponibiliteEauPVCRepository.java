package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface DisponibiliteEauPVCRepository extends CrudRepository<DisponibiliteEauPVC, Integer> {
	DisponibiliteEauPVC findByCode(Integer code);
	DisponibiliteEauPVC findByIsDeletedFalseAndLibelle(String libelle);
	List<DisponibiliteEauPVC> findByIsDeletedFalseOrderByLibelleAsc();
	List<DisponibiliteEauPVC> findByIsDeletedFalse();
	List<DisponibiliteEauPVC> findByIsDeletedTrue();
	List<DisponibiliteEauPVC> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<DisponibiliteEauPVC> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
