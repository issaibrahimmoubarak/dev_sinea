package ne.mha.sinea.referentiel.indicateur.indicateur;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.unite.Unite;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupe;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "indicateur")
public class Indicateur extends CommonProperties {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	
	@Column(name = "libelle",unique=true,columnDefinition="TEXT")
	private String libelle;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "sous_groupe_indicateur", 
        joinColumns = @JoinColumn(
          name = "code_indicateur", referencedColumnName = "code"), 
        inverseJoinColumns = @JoinColumn(
          name = "code_sous_groupe", referencedColumnName = "code"))
    private List<SousGroupe> sousGroupes = new ArrayList<>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "unite_indicateur", 
        joinColumns = @JoinColumn(
          name = "code_indicateur", referencedColumnName = "code"), 
        inverseJoinColumns = @JoinColumn(
          name = "code_unite", referencedColumnName = "code"))
    private List<Unite> unites = new ArrayList<>();
}
