package ne.mha.sinea.nomenclature.unite;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UniteForm {

	private int code;
	private String libelle;
	
}
