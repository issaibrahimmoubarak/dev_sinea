package ne.mha.sinea.nomenclature.typeEquipementStationMeteo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeEquipementStationMeteoForm {

	private int code;
	private String libelle;
	
}
