package ne.mha.sinea.referentiel.zone;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeLocaliteForm {

	private int code;
	private String libelle;
	
}
