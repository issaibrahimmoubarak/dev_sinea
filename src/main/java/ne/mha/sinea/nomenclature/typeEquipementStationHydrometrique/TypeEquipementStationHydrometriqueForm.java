package ne.mha.sinea.nomenclature.typeEquipementStationHydrometrique;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeEquipementStationHydrometriqueForm {

	private int code;
	private String libelle;
	
}
