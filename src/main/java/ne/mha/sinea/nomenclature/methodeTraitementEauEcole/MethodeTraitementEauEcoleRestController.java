package ne.mha.sinea.nomenclature.methodeTraitementEauEcole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MethodeTraitementEauEcoleRestController {

	@Autowired
	MethodeTraitementEauEcoleRepository methodeTraitementEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/methodeTraitementEauEcole")
	public int addMethodeTraitementEauEcole(@Validated MethodeTraitementEauEcoleForm methodeTraitementEauEcoleForm,BindingResult bindingResult, Model model) {
		MethodeTraitementEauEcole savedMethodeTraitementEauEcole = new MethodeTraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MethodeTraitementEauEcole P = new MethodeTraitementEauEcole();
					P.setLibelle(methodeTraitementEauEcoleForm.getLibelle());
					savedMethodeTraitementEauEcole = methodeTraitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateMethodeTraitementEauEcole")
    public int updateMethodeTraitementEauEcole(@Validated MethodeTraitementEauEcoleForm methodeTraitementEauEcoleForm,BindingResult bindingResult, Model model) {
		MethodeTraitementEauEcole savedMethodeTraitementEauEcole = new MethodeTraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MethodeTraitementEauEcole P = methodeTraitementEauEcoleRepository.findByCode(methodeTraitementEauEcoleForm.getCode());
					P.setLibelle(methodeTraitementEauEcoleForm.getLibelle());
					savedMethodeTraitementEauEcole = methodeTraitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteMethodeTraitementEauEcole")
    public int deleteMethodeTraitementEauEcole(@Validated MethodeTraitementEauEcoleForm methodeTraitementEauEcoleForm,BindingResult bindingResult, Model model) {
		MethodeTraitementEauEcole savedMethodeTraitementEauEcole = new MethodeTraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MethodeTraitementEauEcole P = methodeTraitementEauEcoleRepository.findByCode(methodeTraitementEauEcoleForm.getCode());
					P.setIsDeleted(true);
					savedMethodeTraitementEauEcole = methodeTraitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeTraitementEauEcole.getCode();
		
        
    }
}
