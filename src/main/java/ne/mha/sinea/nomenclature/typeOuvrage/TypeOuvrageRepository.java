package ne.mha.sinea.nomenclature.typeOuvrage;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeOuvrageRepository extends CrudRepository<TypeOuvrage, Integer> {
	TypeOuvrage findByCode(Integer code);
	TypeOuvrage findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeOuvrage> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeOuvrage> findByIsDeletedFalse();
	List<TypeOuvrage> findByIsDeletedTrue();
	List<TypeOuvrage> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeOuvrage> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
