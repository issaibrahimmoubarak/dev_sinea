package ne.mha.sinea.nomenclature.raisonNonTraitementEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class RaisonNonTraitementEauRestController {

	@Autowired
	RaisonNonTraitementEauRepository raisonNonTraitementEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/raisonNonTraitementEau")
	public int addRaisonNonTraitementEau(@Validated RaisonNonTraitementEauForm raisonNonTraitementEauForm,BindingResult bindingResult, Model model) {
		RaisonNonTraitementEau savedRaisonNonTraitementEau = new RaisonNonTraitementEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					RaisonNonTraitementEau P = new RaisonNonTraitementEau();
					P.setLibelle(raisonNonTraitementEauForm.getLibelle());
					savedRaisonNonTraitementEau = raisonNonTraitementEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRaisonNonTraitementEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateRaisonNonTraitementEau")
    public int updateRaisonNonTraitementEau(@Validated RaisonNonTraitementEauForm raisonNonTraitementEauForm,BindingResult bindingResult, Model model) {
		RaisonNonTraitementEau savedRaisonNonTraitementEau = new RaisonNonTraitementEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					RaisonNonTraitementEau P = raisonNonTraitementEauRepository.findByCode(raisonNonTraitementEauForm.getCode());
					P.setLibelle(raisonNonTraitementEauForm.getLibelle());
					savedRaisonNonTraitementEau = raisonNonTraitementEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRaisonNonTraitementEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteRaisonNonTraitementEau")
    public int deleteRaisonNonTraitementEau(@Validated RaisonNonTraitementEauForm raisonNonTraitementEauForm,BindingResult bindingResult, Model model) {
		RaisonNonTraitementEau savedRaisonNonTraitementEau = new RaisonNonTraitementEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					RaisonNonTraitementEau P = raisonNonTraitementEauRepository.findByCode(raisonNonTraitementEauForm.getCode());
					P.setIsDeleted(true);
					savedRaisonNonTraitementEau = raisonNonTraitementEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRaisonNonTraitementEau.getCode();
		
        
    }
}
