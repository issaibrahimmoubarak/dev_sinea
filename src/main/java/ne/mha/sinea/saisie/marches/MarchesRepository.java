package ne.mha.sinea.saisie.marches;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface MarchesRepository extends CrudRepository<Marches, Integer> {
    
    Marches findByCode(Integer code);
    Marches findByObjet(String objet);
    List<Marches> findByIsDeletedFalse();
    List<Marches> findByIsDeletedTrue();
}
