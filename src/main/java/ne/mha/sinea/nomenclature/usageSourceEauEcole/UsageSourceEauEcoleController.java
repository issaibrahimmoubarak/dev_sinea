package ne.mha.sinea.nomenclature.usageSourceEauEcole;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class UsageSourceEauEcoleController {

	@Autowired
	UsageSourceEauEcoleRepository usageSourceEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/usageSourceEauEcole")
	public String  addUsageSourceEauEcole(UsageSourceEauEcoleForm usageSourceEauEcoleForm, Model model) {
		try{
			List<UsageSourceEauEcole> usageSourceEauEcole = usageSourceEauEcoleRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("usageSourceEauEcole", usageSourceEauEcole);
			
			model.addAttribute("viewPath", "nomenclature/usageSourceEauEcole/usageSourceEauEcole");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
