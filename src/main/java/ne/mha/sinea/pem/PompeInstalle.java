package ne.mha.sinea.pem;

//import java.sql.Date;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.marquePompe.MarquePompe;
import ne.mha.sinea.nomenclature.modelePompe.ModelePompe;
import ne.mha.sinea.nomenclature.natureTube.NatureTube;
import ne.mha.sinea.nomenclature.typeTube.TypeTube;

@Data
@Entity
@Table(name = "pompe_installe")
public class PompeInstalle extends CommonProperties {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	private Integer code;
	
	@JoinColumn(name = "code_pem", referencedColumnName = "code")
	@ManyToOne
	private PEM pem;
	@JoinColumn(name = "code_modele_pompe", referencedColumnName = "code")
	@ManyToOne
	private ModelePompe modelePompe;
	@JoinColumn(name = "code_marque_pompe", referencedColumnName = "code")
	@ManyToOne
	private MarquePompe marquePompe;
	@Column(name = "date_installation_pompe")
	private Date dateInstallationPompe;
	
}
