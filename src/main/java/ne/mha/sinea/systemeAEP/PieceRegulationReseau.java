package ne.mha.sinea.systemeAEP;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.typePieceRegulationReseau.TypePieceRegulationReseau;

@Data
@Entity
@Table(name = "piece_regulation_reseau")
public class PieceRegulationReseau extends CommonProperties{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@Column(name = "numero_ordre")
	protected Integer numeroOrdre;
	@Column(name = "latitude")
	protected Double latitude;
	@Column(name = "longitude")
	protected Double longitude;
	@JoinColumn(name = "type_piece_regulation_reseau", referencedColumnName = "code")
	@ManyToOne
	protected TypePieceRegulationReseau typePieceRegulationReseau;
	@JoinColumn(name = "code_systeme_aep", referencedColumnName = "code")
	@ManyToOne
	protected SystemeAEP systemeAEP;
}
