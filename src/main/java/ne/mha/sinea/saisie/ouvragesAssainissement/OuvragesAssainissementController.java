package ne.mha.sinea.saisie.ouvragesAssainissement;

import java.util.List;

import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;

import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
import ne.mha.sinea.Template;

import ne.mha.sinea.nomenclature.lieu.Lieu;
import ne.mha.sinea.nomenclature.lieu.LieuRepository;
import ne.mha.sinea.nomenclature.typeLatrines.TypeLatrines;
import ne.mha.sinea.nomenclature.typeLatrines.TypeLatrinesRepository;
import ne.mha.sinea.nomenclature.typeOuvrageAssainissement.TypeOuvrageAssainissement;
import ne.mha.sinea.nomenclature.typeOuvrageAssainissement.TypeOuvrageAssainissementRepository;

@Controller
public class OuvragesAssainissementController {

	@Autowired
	EntityManager em;

	@Autowired
	OuvragesAssainissementRepository ouvragesAssainissementService;
	@Autowired
	LieuRepository lieuService;
	@Autowired
	TypeLatrinesRepository typeLatrinesService;
	@Autowired
	TypeOuvrageAssainissementRepository typeOuvrageAssainissementService;

	@GetMapping("/ouvragesAssainissement")
	public String addOuvragesAssainissement(OuvragesAssainissementForm ouvragesAssainissementForm, Model model) {
		try {
			List<OuvragesAssainissement> ouvragesAssainissement = ouvragesAssainissementService.findByIsDeletedFalse();
			model.addAttribute("ouvragesAssainissement", ouvragesAssainissement);
			List<Lieu> lieu = lieuService.findByIsDeletedFalse();
			model.addAttribute("lieu", lieu);
			List<TypeLatrines> typeLatrine = typeLatrinesService.findByIsDeletedFalse();
			model.addAttribute("typeLatrine", typeLatrine);
			List<TypeOuvrageAssainissement> typeOuvrageAssainissement = typeOuvrageAssainissementService
					.findByIsDeletedFalse();
			model.addAttribute("typeOuvrageAssainissement", typeOuvrageAssainissement);

			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			// model.addAttribute("navigationPath", "Saisie/Ouvrage");
			model.addAttribute("viewPath", "saisie/ouvragesAssainissement/ouvragesAssainissement");

		} catch (Exception e) {

		}
		return Template.secondTemplate;

	}

	// validation du formulaire d'ajout OuvragesAssainissement
	@PostMapping("/ouvragesAssainissement")
	public String addOuvragesAssainissementsSubmit(@Validated OuvragesAssainissementForm ouvragesAssainissementForm,
			BindingResult bindingResult,
			Model model) {
		// s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {

			try {
				// récuperer les données saisies
				OuvragesAssainissement OA = new OuvragesAssainissement();
				OA.setCodeINS(ouvragesAssainissementForm.getCodeINS());
				OA.setIndentification(ouvragesAssainissementForm.getIndentification());
				OA.setNbDispositifInterieur(ouvragesAssainissementForm.getNbDispositifInterieur());
				OA.setNbDispositifEnceinte(ouvragesAssainissementForm.getNbDispositifEnceinte());
				OA.setLieu(lieuService.findByCode(ouvragesAssainissementForm.getCodeLieu()));
				OA.setTypeLatrines(typeLatrinesService.findByCode(ouvragesAssainissementForm.getCodeTypeLatrines()));
				OA.setTypeOuvrageAssainissement(typeOuvrageAssainissementService
						.findByCode(ouvragesAssainissementForm.getCodeTypeOuvrageAssainissement()));
				ouvragesAssainissementService.save(OA);

				// indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				List<OuvragesAssainissement> ouvragesAssainissement = ouvragesAssainissementService
						.findByIsDeletedFalse();
				model.addAttribute("ouvragesAssainissement", ouvragesAssainissement);
				List<Lieu> lieu = lieuService.findByIsDeletedFalse();
				model.addAttribute("lieu", lieu);
				List<TypeLatrines> typeLatrine = typeLatrinesService.findByIsDeletedFalse();
				model.addAttribute("typeLatrine", typeLatrine);
				List<TypeOuvrageAssainissement> typeOuvrageAssainissement = typeOuvrageAssainissementService
						.findByIsDeletedFalse();
				model.addAttribute("typeOuvrageAssainissement", typeOuvrageAssainissement);

				// model.addAttribute("navigationPath", "Saisie/Ouvrage");
				model.addAttribute("viewPath", "saisie/ouvragesAssainissement/ouvragesAssainissement");

			} catch (Exception e) {

			}

		} else {
			try {

			} catch (Exception e) {

			}
		}

		// afficher le template avec la vue (formulaire) à l'intérieur
		return Template.secondTemplate;

	}

	// modification d'une ouvrage
	@GetMapping("/updateOuvragesAssainissement/{code}")
	public String updateOuvragesAssainissement(@PathVariable("code") int code,
			OuvragesAssainissementForm ouvragesAssainissementForm, Model model) {
		try {
			// récuperer les informations d'ouvrage à modifier
			OuvragesAssainissement ouvragesAssainissement = ouvragesAssainissementService.findByCode(code);
			// envoie des informations d'ouvrage à modifier à travers le modele
			ouvragesAssainissementForm.setCodeINS(ouvragesAssainissementForm.getCodeINS());
			ouvragesAssainissementForm.setIndentification(ouvragesAssainissementForm.getIndentification());
			ouvragesAssainissementForm.setNbDispositifInterieur(ouvragesAssainissementForm.getNbDispositifInterieur());
			ouvragesAssainissementForm.setNbDispositifEnceinte(ouvragesAssainissementForm.getNbDispositifEnceinte());
			ouvragesAssainissementForm.setCodeLieu(ouvragesAssainissement.getLieu().getCode());
			ouvragesAssainissementForm.setCodeTypeLatrines(ouvragesAssainissement.getTypeLatrines().getCode());
			ouvragesAssainissementForm
					.setCodeTypeOuvrageAssainissement(ouvragesAssainissement.getTypeOuvrageAssainissement().getCode());
			// récuperation de la liste des ouvrages de la base de données
			List<OuvragesAssainissement> ouvragesAssainissements = ouvragesAssainissementService.findByIsDeletedFalse();
			model.addAttribute("ouvragesAssainissements", ouvragesAssainissements);
			List<Lieu> lieu = lieuService.findByIsDeletedFalse();
			model.addAttribute("lieu", lieu);
			List<TypeLatrines> typeLatrine = typeLatrinesService.findByIsDeletedFalse();
			model.addAttribute("typeLatrine", typeLatrine);
			List<TypeOuvrageAssainissement> typeOuvrageAssainissement = typeOuvrageAssainissementService
					.findByIsDeletedFalse();
			model.addAttribute("typeOuvrageAssainissement", typeOuvrageAssainissement);
			model.addAttribute("viewPath", "saisie/ouvragesAssainissement/updateOuvragesAssainissement");

		} catch (Exception e) {

		}
		return Template.secondTemplate;

	}

	@PostMapping("/updateOuvragesAssainissement/{code}")
	public RedirectView updateOuvragesAssainissementSubmit(
			@Validated OuvragesAssainissementForm ouvragesAssainissementForm, BindingResult bindingResult,
			@PathVariable("code") int code, Model model, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/ouvragesAssainissement", true);
		if (!bindingResult.hasErrors()) {
			try {
				// recupérer les informations saisies
				OuvragesAssainissement OA = ouvragesAssainissementService.findByCode(code);
				OA.setCodeINS(ouvragesAssainissementForm.getCodeINS());
				OA.setIndentification(ouvragesAssainissementForm.getIndentification());
				OA.setNbDispositifInterieur(ouvragesAssainissementForm.getNbDispositifInterieur());
				OA.setNbDispositifEnceinte(ouvragesAssainissementForm.getNbDispositifEnceinte());
				OA.setLieu(lieuService.findByCode(ouvragesAssainissementForm.getCodeLieu()));
				OA.setTypeLatrines(typeLatrinesService.findByCode(ouvragesAssainissementForm.getCodeTypeLatrines()));
				OA.setTypeOuvrageAssainissement(typeOuvrageAssainissementService
						.findByCode(ouvragesAssainissementForm.getCodeTypeOuvrageAssainissement()));
				// enregistrer les informations dans la base de données
				ouvragesAssainissementService.save(OA);
				// indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");

			} catch (Exception e) {
				// indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
		} else {

		}

		// redirection vers la page d'ajout
		return redirectView;

	}

	@GetMapping("/deleteOuvragesAssainissement/{code}")
	public RedirectView deleteOuvragesAssainissement(@PathVariable("code") int code, Model model,
			RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/ouvragesAssainissement", true);
		try {
			OuvragesAssainissement OA = ouvragesAssainissementService.findByCode(code);
			OA.setIsDeleted(true);
			ouvragesAssainissementService.save(OA);
			;

			// indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		} catch (Exception e) {
			// indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		// redirection vers la page d'ajout
		return redirectView;

	}

}
