package ne.mha.sinea.nomenclature.etatPointEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class EtatPointEauRestController {

	@Autowired
	EtatPointEauRepository etatPointEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/etatPointEau")
	public int addEtatPointEau(@Validated EtatPointEauForm etatPointEauForm,BindingResult bindingResult, Model model) {
		EtatPointEau savedEtatPointEau = new EtatPointEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatPointEau P = new EtatPointEau();
					P.setLibelle(etatPointEauForm.getLibelle());
					savedEtatPointEau = etatPointEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatPointEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateEtatPointEau")
    public int updateEtatPointEau(@Validated EtatPointEauForm etatPointEauForm,BindingResult bindingResult, Model model) {
		EtatPointEau savedEtatPointEau = new EtatPointEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatPointEau P = etatPointEauRepository.findByCode(etatPointEauForm.getCode());
					P.setLibelle(etatPointEauForm.getLibelle());
					savedEtatPointEau = etatPointEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatPointEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteEtatPointEau")
    public int deleteEtatPointEau(@Validated EtatPointEauForm etatPointEauForm,BindingResult bindingResult, Model model) {
		EtatPointEau savedEtatPointEau = new EtatPointEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatPointEau P = etatPointEauRepository.findByCode(etatPointEauForm.getCode());
					P.setIsDeleted(true);
					savedEtatPointEau = etatPointEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatPointEau.getCode();
		
        
    }
}
