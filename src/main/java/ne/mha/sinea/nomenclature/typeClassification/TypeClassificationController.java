package ne.mha.sinea.nomenclature.typeClassification;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ne.mha.sinea.Template;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrame;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrameRepository;

@Controller
public class TypeClassificationController {
	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	@Autowired
	SectionFrameRepository sectionFrameService;
	
	//@PreAuthorize("hasAuthority('gestion des types de classification')")
	@GetMapping("/typeClassification")
	public String zone(Model model){
		List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
		model.addAttribute("typeClassifications", typeClassifications);

		model.addAttribute("viewPath", "nomenclature/typeClassification/typeClassification");
		
		return Template.nomenclatureTemplate;
	}
}
