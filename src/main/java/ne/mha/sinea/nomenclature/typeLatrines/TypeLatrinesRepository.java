package ne.mha.sinea.nomenclature.typeLatrines;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeLatrinesRepository extends CrudRepository<TypeLatrines, Integer> {
	TypeLatrines findByCode(Integer code);
	TypeLatrines findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeLatrines> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeLatrines> findByIsDeletedFalse();
	List<TypeLatrines> findByIsDeletedTrue();
	List<TypeLatrines> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeLatrines> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
