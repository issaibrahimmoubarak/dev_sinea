package ne.mha.sinea.nomenclature.traitementEauEcole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TraitementEauEcoleRestController {

	@Autowired
	TraitementEauEcoleRepository traitementEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/traitementEauEcole")
	public int addTraitementEauEcole(@Validated TraitementEauEcoleForm traitementEauEcoleForm,BindingResult bindingResult, Model model) {
		TraitementEauEcole savedTraitementEauEcole = new TraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TraitementEauEcole P = new TraitementEauEcole();
					P.setLibelle(traitementEauEcoleForm.getLibelle());
					savedTraitementEauEcole = traitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTraitementEauEcole")
    public int updateTraitementEauEcole(@Validated TraitementEauEcoleForm traitementEauEcoleForm,BindingResult bindingResult, Model model) {
		TraitementEauEcole savedTraitementEauEcole = new TraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TraitementEauEcole P = traitementEauEcoleRepository.findByCode(traitementEauEcoleForm.getCode());
					P.setLibelle(traitementEauEcoleForm.getLibelle());
					savedTraitementEauEcole = traitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTraitementEauEcole")
    public int deleteTraitementEauEcole(@Validated TraitementEauEcoleForm traitementEauEcoleForm,BindingResult bindingResult, Model model) {
		TraitementEauEcole savedTraitementEauEcole = new TraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TraitementEauEcole P = traitementEauEcoleRepository.findByCode(traitementEauEcoleForm.getCode());
					P.setIsDeleted(true);
					savedTraitementEauEcole = traitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTraitementEauEcole.getCode();
		
        
    }
}
