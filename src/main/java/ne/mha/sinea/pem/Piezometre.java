package ne.mha.sinea.pem;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;

@Data
@Entity
@Table(name = "piezometre")
public class Piezometre extends CommonProperties{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	private Integer code;
	@ManyToOne
	private PEM pem;
	@JoinColumn(name = "code_pem", referencedColumnName = "code")
	@Column(name = "date_observation")
	private Date dateObservation;
	@Column(name = "heure_observation")
	private String heureObservation;
	@Column(name = "profondeur_piezo")
	private Double profondeurPiezo;
	@Column(name = "hauteur_piezo")
	private Double hauteurPiezo;
}
