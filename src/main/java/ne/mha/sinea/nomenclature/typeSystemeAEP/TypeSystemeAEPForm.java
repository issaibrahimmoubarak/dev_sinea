package ne.mha.sinea.nomenclature.typeSystemeAEP;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeSystemeAEPForm {

	private int code;
	private String libelle;
	
}
