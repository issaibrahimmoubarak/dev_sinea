package ne.mha.sinea.nomenclature.natureTube;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface NatureTubeRepository extends CrudRepository<NatureTube, Integer> {
	NatureTube findByCode(Integer code);
	NatureTube findByIsDeletedFalseAndLibelle(String libelle);
	List<NatureTube> findByIsDeletedFalseOrderByLibelleAsc();
	List<NatureTube> findByIsDeletedFalse();
	List<NatureTube> findByIsDeletedTrue();
	List<NatureTube> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<NatureTube> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
