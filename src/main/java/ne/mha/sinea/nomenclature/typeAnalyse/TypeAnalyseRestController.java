package ne.mha.sinea.nomenclature.typeAnalyse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeAnalyseRestController {

	@Autowired
	TypeAnalyseRepository typeAnalyseService;
	
	//@PreAuthorize("hasAuthority('gestion des typeAnalyse')")
	@PostMapping("/typeAnalyse")
	public int addTypeAnalyse(@Validated TypeAnalyseForm typeAnalyseForm,BindingResult bindingResult, Model model) {
		TypeAnalyse savedTypeAnalyse = new TypeAnalyse();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeAnalyse P = new TypeAnalyse();
					P.setLibelle(typeAnalyseForm.getLibelle());
					savedTypeAnalyse = typeAnalyseService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAnalyse.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeAnalyse")
    public int updateTypeAnalyse(@Validated TypeAnalyseForm typeAnalyseForm,BindingResult bindingResult, Model model) {
		TypeAnalyse savedTypeAnalyse = new TypeAnalyse();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeAnalyse P = typeAnalyseService.findByCode(typeAnalyseForm.getCode());
					P.setLibelle(typeAnalyseForm.getLibelle());
					savedTypeAnalyse = typeAnalyseService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAnalyse.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeAnalyse")
    public int deleteTypeAnalyse(@Validated TypeAnalyseForm typeAnalyseForm,BindingResult bindingResult, Model model) {
		TypeAnalyse savedTypeAnalyse = new TypeAnalyse();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeAnalyse P = typeAnalyseService.findByCode(typeAnalyseForm.getCode());
					P.setIsDeleted(true);
					savedTypeAnalyse = typeAnalyseService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAnalyse.getCode();
		
        
    }
}
