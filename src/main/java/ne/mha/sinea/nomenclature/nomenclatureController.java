package ne.mha.sinea.nomenclature;

import ne.mha.sinea.Template;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;


@Controller
public class nomenclatureController {

    @GetMapping("/nomenclature")
	public String PointEau(Model model) {
        try {
            model.addAttribute("viewPath", "/nomenclature/home");
        } catch (Exception e) {
            System.out.println("erreur "+e);	
        }
		return Template.nomenclatureTemplate;
	}
}
