package ne.mha.sinea.nomenclature.typeBranchement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeBranchementRestController {

	@Autowired
	TypeBranchementRepository typeBranchementService;
	
	//@PreAuthorize("hasAuthority('gestion des typeBranchement')")
	@PostMapping("/typeBranchement")
	public int addTypeBranchement(@Validated TypeBranchementForm typeBranchementForm,BindingResult bindingResult, Model model) {
		TypeBranchement savedTypeBranchement = new TypeBranchement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeBranchement P = new TypeBranchement();
					P.setLibelle(typeBranchementForm.getLibelle());
					savedTypeBranchement = typeBranchementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeBranchement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeBranchement")
    public int updateTypeBranchement(@Validated TypeBranchementForm typeBranchementForm,BindingResult bindingResult, Model model) {
		TypeBranchement savedTypeBranchement = new TypeBranchement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeBranchement P = typeBranchementService.findByCode(typeBranchementForm.getCode());
					P.setLibelle(typeBranchementForm.getLibelle());
					savedTypeBranchement = typeBranchementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeBranchement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeBranchement")
    public int deleteTypeBranchement(@Validated TypeBranchementForm typeBranchementForm,BindingResult bindingResult, Model model) {
		TypeBranchement savedTypeBranchement = new TypeBranchement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeBranchement P = typeBranchementService.findByCode(typeBranchementForm.getCode());
					P.setIsDeleted(true);
					savedTypeBranchement = typeBranchementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeBranchement.getCode();
		
        
    }
}
