package ne.mha.sinea.nomenclature.typeEquipement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeEquipementRestController {

	@Autowired
	TypeEquipementRepository typeEquipementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeEquipement")
	public int addTypeEquipement(@Validated TypeEquipementForm typeEquipementForm,BindingResult bindingResult, Model model) {
		TypeEquipement savedTypeEquipement = new TypeEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipement P = new TypeEquipement();
					P.setLibelle(typeEquipementForm.getLibelle());
					savedTypeEquipement = typeEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeEquipement")
    public int updateTypeEquipement(@Validated TypeEquipementForm typeEquipementForm,BindingResult bindingResult, Model model) {
		TypeEquipement savedTypeEquipement = new TypeEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipement P = typeEquipementRepository.findByCode(typeEquipementForm.getCode());
					P.setLibelle(typeEquipementForm.getLibelle());
					savedTypeEquipement = typeEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeEquipement")
    public int deleteTypeEquipement(@Validated TypeEquipementForm typeEquipementForm,BindingResult bindingResult, Model model) {
		TypeEquipement savedTypeEquipement = new TypeEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipement P = typeEquipementRepository.findByCode(typeEquipementForm.getCode());
					P.setIsDeleted(true);
					savedTypeEquipement = typeEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipement.getCode();
		
        
    }
}
