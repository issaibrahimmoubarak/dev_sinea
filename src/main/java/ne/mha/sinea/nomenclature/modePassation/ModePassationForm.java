package ne.mha.sinea.nomenclature.modePassation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModePassationForm {

	private int code;
	private String libelle;
	
}
