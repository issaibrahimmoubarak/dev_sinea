package ne.mha.sinea.nomenclature.propriete;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProprieteForm {

	private int code;
	private String libelle;
	
}
