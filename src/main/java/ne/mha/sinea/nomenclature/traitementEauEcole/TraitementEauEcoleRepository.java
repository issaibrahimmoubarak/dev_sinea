package ne.mha.sinea.nomenclature.traitementEauEcole;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TraitementEauEcoleRepository extends CrudRepository<TraitementEauEcole, Integer> {
	TraitementEauEcole findByCode(Integer code);
	TraitementEauEcole findByIsDeletedFalseAndLibelle(String libelle);
	List<TraitementEauEcole> findByIsDeletedFalseOrderByLibelleAsc();
	List<TraitementEauEcole> findByIsDeletedFalse();
	List<TraitementEauEcole> findByIsDeletedTrue();
	List<TraitementEauEcole> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TraitementEauEcole> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
