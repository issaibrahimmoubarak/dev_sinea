package ne.mha.sinea.nomenclature.marqueModuleMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MarqueModuleMiniAEPRestController {

	@Autowired
	MarqueModuleMiniAEPRepository marqueModuleMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/marqueModuleMiniAEP")
	public int addMarqueModuleMiniAEP(@Validated MarqueModuleMiniAEPForm marqueModuleMiniAEPForm,BindingResult bindingResult, Model model) {
		MarqueModuleMiniAEP savedMarqueModuleMiniAEP = new MarqueModuleMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarqueModuleMiniAEP P = new MarqueModuleMiniAEP();
					P.setLibelle(marqueModuleMiniAEPForm.getLibelle());
					savedMarqueModuleMiniAEP = marqueModuleMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueModuleMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateMarqueModuleMiniAEP")
    public int updateMarqueModuleMiniAEP(@Validated MarqueModuleMiniAEPForm marqueModuleMiniAEPForm,BindingResult bindingResult, Model model) {
		MarqueModuleMiniAEP savedMarqueModuleMiniAEP = new MarqueModuleMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarqueModuleMiniAEP P = marqueModuleMiniAEPRepository.findByCode(marqueModuleMiniAEPForm.getCode());
					P.setLibelle(marqueModuleMiniAEPForm.getLibelle());
					savedMarqueModuleMiniAEP = marqueModuleMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueModuleMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteMarqueModuleMiniAEP")
    public int deleteMarqueModuleMiniAEP(@Validated MarqueModuleMiniAEPForm marqueModuleMiniAEPForm,BindingResult bindingResult, Model model) {
		MarqueModuleMiniAEP savedMarqueModuleMiniAEP = new MarqueModuleMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarqueModuleMiniAEP P = marqueModuleMiniAEPRepository.findByCode(marqueModuleMiniAEPForm.getCode());
					P.setIsDeleted(true);
					savedMarqueModuleMiniAEP = marqueModuleMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueModuleMiniAEP.getCode();
		
        
    }
}
