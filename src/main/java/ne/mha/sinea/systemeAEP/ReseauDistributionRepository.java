package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ReseauDistributionRepository extends CrudRepository<ReseauDistribution, Integer> {
	
	ReseauDistribution findByCode(Integer code);
	List<ReseauDistribution> findBySystemeAEP_NumeroIRH(String IRH);

}