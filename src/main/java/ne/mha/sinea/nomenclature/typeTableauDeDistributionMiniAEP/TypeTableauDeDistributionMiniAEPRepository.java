package ne.mha.sinea.nomenclature.typeTableauDeDistributionMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeTableauDeDistributionMiniAEPRepository extends CrudRepository<TypeTableauDeDistributionMiniAEP, Integer> {
	TypeTableauDeDistributionMiniAEP findByCode(Integer code);
	TypeTableauDeDistributionMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeTableauDeDistributionMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeTableauDeDistributionMiniAEP> findByIsDeletedFalse();
	List<TypeTableauDeDistributionMiniAEP> findByIsDeletedTrue();
	List<TypeTableauDeDistributionMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeTableauDeDistributionMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
