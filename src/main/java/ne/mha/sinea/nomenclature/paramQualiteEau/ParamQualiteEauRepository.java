package ne.mha.sinea.nomenclature.paramQualiteEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface ParamQualiteEauRepository extends CrudRepository<ParamQualiteEau, Integer> {
	ParamQualiteEau findByCode(Integer code);
	ParamQualiteEau findByIsDeletedFalseAndLibelle(String libelle);
	List<ParamQualiteEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<ParamQualiteEau> findByIsDeletedFalse();
	List<ParamQualiteEau> findByIsDeletedTrue();
	List<ParamQualiteEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<ParamQualiteEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
