package ne.mha.sinea.nomenclature.typePuits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypePuitsRestController {

	@Autowired
	TypePuitsRepository typePuitsService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typePuits")
	public int addTypePuits(@Validated TypePuitsForm typePuitsForm,BindingResult bindingResult, Model model) {
		TypePuits savedTypePuits = new TypePuits();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePuits P = new TypePuits();
					P.setLibelle(typePuitsForm.getLibelle());
					savedTypePuits = typePuitsService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePuits.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypePuits")
    public int updateTypePuits(@Validated TypePuitsForm typePuitsForm,BindingResult bindingResult, Model model) {
		TypePuits savedTypePuits = new TypePuits();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePuits P = typePuitsService.findByCode(typePuitsForm.getCode());
					P.setLibelle(typePuitsForm.getLibelle());
					savedTypePuits = typePuitsService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePuits.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypePuits")
    public int deleteTypePuits(@Validated TypePuitsForm typePuitsForm,BindingResult bindingResult, Model model) {
		TypePuits savedTypePuits = new TypePuits();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePuits P = typePuitsService.findByCode(typePuitsForm.getCode());
					P.setIsDeleted(true);
					savedTypePuits = typePuitsService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePuits.getCode();
		
        
    }
}
