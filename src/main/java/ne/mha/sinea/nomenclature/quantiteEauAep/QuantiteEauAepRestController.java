package ne.mha.sinea.nomenclature.quantiteEauAep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class QuantiteEauAepRestController {

	@Autowired
	QuantiteEauAepRepository quantiteEauAepRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/quantiteEauAep")
	public int addQuantiteEauAep(@Validated QuantiteEauAepForm quantiteEauAepForm,BindingResult bindingResult, Model model) {
		QuantiteEauAep savedQuantiteEauAep = new QuantiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					QuantiteEauAep P = new QuantiteEauAep();
					P.setLibelle(quantiteEauAepForm.getLibelle());
					savedQuantiteEauAep = quantiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQuantiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateQuantiteEauAep")
    public int updateQuantiteEauAep(@Validated QuantiteEauAepForm quantiteEauAepForm,BindingResult bindingResult, Model model) {
		QuantiteEauAep savedQuantiteEauAep = new QuantiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					QuantiteEauAep P = quantiteEauAepRepository.findByCode(quantiteEauAepForm.getCode());
					P.setLibelle(quantiteEauAepForm.getLibelle());
					savedQuantiteEauAep = quantiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQuantiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteQuantiteEauAep")
    public int deleteQuantiteEauAep(@Validated QuantiteEauAepForm quantiteEauAepForm,BindingResult bindingResult, Model model) {
		QuantiteEauAep savedQuantiteEauAep = new QuantiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					QuantiteEauAep P = quantiteEauAepRepository.findByCode(quantiteEauAepForm.getCode());
					P.setIsDeleted(true);
					savedQuantiteEauAep = quantiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQuantiteEauAep.getCode();
		
        
    }
}
