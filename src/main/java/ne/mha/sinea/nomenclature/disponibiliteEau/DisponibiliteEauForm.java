package ne.mha.sinea.nomenclature.disponibiliteEau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DisponibiliteEauForm {

	private int code;
	private String libelle;
	
}
