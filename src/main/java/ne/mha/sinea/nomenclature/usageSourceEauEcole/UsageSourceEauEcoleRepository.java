package ne.mha.sinea.nomenclature.usageSourceEauEcole;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface UsageSourceEauEcoleRepository extends CrudRepository<UsageSourceEauEcole, Integer> {
	UsageSourceEauEcole findByCode(Integer code);
	UsageSourceEauEcole findByIsDeletedFalseAndLibelle(String libelle);
	List<UsageSourceEauEcole> findByIsDeletedFalseOrderByLibelleAsc();
	List<UsageSourceEauEcole> findByIsDeletedFalse();
	List<UsageSourceEauEcole> findByIsDeletedTrue();
	List<UsageSourceEauEcole> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<UsageSourceEauEcole> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
