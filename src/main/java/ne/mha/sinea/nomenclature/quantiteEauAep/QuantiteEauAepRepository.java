package ne.mha.sinea.nomenclature.quantiteEauAep;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface QuantiteEauAepRepository extends CrudRepository<QuantiteEauAep, Integer> {
	QuantiteEauAep findByCode(Integer code);
	QuantiteEauAep findByIsDeletedFalseAndLibelle(String libelle);
	List<QuantiteEauAep> findByIsDeletedFalseOrderByLibelleAsc();
	List<QuantiteEauAep> findByIsDeletedFalse();
	List<QuantiteEauAep> findByIsDeletedTrue();
	List<QuantiteEauAep> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<QuantiteEauAep> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
