package ne.mha.sinea.nomenclature.etatCompteurForageMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface EtatCompteurForageMiniAEPRepository extends CrudRepository<EtatCompteurForageMiniAEP, Integer> {
	EtatCompteurForageMiniAEP findByCode(Integer code);
	EtatCompteurForageMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<EtatCompteurForageMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<EtatCompteurForageMiniAEP> findByIsDeletedFalse();
	List<EtatCompteurForageMiniAEP> findByIsDeletedTrue();
	List<EtatCompteurForageMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<EtatCompteurForageMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
