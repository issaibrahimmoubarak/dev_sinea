package ne.mha.sinea.nomenclature.typePrestationEquipement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypePrestationEquipementRestController {

	@Autowired
	TypePrestationEquipementRepository typePrestationEquipementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typePrestationEquipement")
	public int addTypePrestationEquipement(@Validated TypePrestationEquipementForm typePrestationEquipementForm,BindingResult bindingResult, Model model) {
		TypePrestationEquipement savedTypePrestationEquipement = new TypePrestationEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestationEquipement P = new TypePrestationEquipement();
					P.setLibelle(typePrestationEquipementForm.getLibelle());
					savedTypePrestationEquipement = typePrestationEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationEquipement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypePrestationEquipement")
    public int updateTypePrestationEquipement(@Validated TypePrestationEquipementForm typePrestationEquipementForm,BindingResult bindingResult, Model model) {
		TypePrestationEquipement savedTypePrestationEquipement = new TypePrestationEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestationEquipement P = typePrestationEquipementRepository.findByCode(typePrestationEquipementForm.getCode());
					P.setLibelle(typePrestationEquipementForm.getLibelle());
					savedTypePrestationEquipement = typePrestationEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationEquipement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypePrestationEquipement")
    public int deleteTypePrestationEquipement(@Validated TypePrestationEquipementForm typePrestationEquipementForm,BindingResult bindingResult, Model model) {
		TypePrestationEquipement savedTypePrestationEquipement = new TypePrestationEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestationEquipement P = typePrestationEquipementRepository.findByCode(typePrestationEquipementForm.getCode());
					P.setIsDeleted(true);
					savedTypePrestationEquipement = typePrestationEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationEquipement.getCode();
		
        
    }
}
