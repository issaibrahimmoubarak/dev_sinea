package ne.mha.sinea.nomenclature.typeRessourceStationHydrometrique;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeRessourceStationHydrometriqueRepository extends CrudRepository<TypeRessourceStationHydrometrique, Integer> {
	TypeRessourceStationHydrometrique findByCode(Integer code);
	TypeRessourceStationHydrometrique findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeRessourceStationHydrometrique> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeRessourceStationHydrometrique> findByIsDeletedFalse();
	List<TypeRessourceStationHydrometrique> findByIsDeletedTrue();
	List<TypeRessourceStationHydrometrique> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeRessourceStationHydrometrique> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
