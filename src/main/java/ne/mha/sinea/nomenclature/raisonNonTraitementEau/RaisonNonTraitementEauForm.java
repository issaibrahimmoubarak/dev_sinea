package ne.mha.sinea.nomenclature.raisonNonTraitementEau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RaisonNonTraitementEauForm {

	private int code;
	private String libelle;
	
}
