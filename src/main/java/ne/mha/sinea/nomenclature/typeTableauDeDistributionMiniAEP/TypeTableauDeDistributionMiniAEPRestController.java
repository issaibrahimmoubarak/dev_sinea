package ne.mha.sinea.nomenclature.typeTableauDeDistributionMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeTableauDeDistributionMiniAEPRestController {

	@Autowired
	TypeTableauDeDistributionMiniAEPRepository typeTableauDeDistributionMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeTableauDeDistributionMiniAEP")
	public int addTypeTableauDeDistributionMiniAEP(@Validated TypeTableauDeDistributionMiniAEPForm typeTableauDeDistributionMiniAEPForm,BindingResult bindingResult, Model model) {
		TypeTableauDeDistributionMiniAEP savedTypeTableauDeDistributionMiniAEP = new TypeTableauDeDistributionMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeTableauDeDistributionMiniAEP P = new TypeTableauDeDistributionMiniAEP();
					P.setLibelle(typeTableauDeDistributionMiniAEPForm.getLibelle());
					savedTypeTableauDeDistributionMiniAEP = typeTableauDeDistributionMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTableauDeDistributionMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeTableauDeDistributionMiniAEP")
    public int updateTypeTableauDeDistributionMiniAEP(@Validated TypeTableauDeDistributionMiniAEPForm typeTableauDeDistributionMiniAEPForm,BindingResult bindingResult, Model model) {
		TypeTableauDeDistributionMiniAEP savedTypeTableauDeDistributionMiniAEP = new TypeTableauDeDistributionMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeTableauDeDistributionMiniAEP P = typeTableauDeDistributionMiniAEPRepository.findByCode(typeTableauDeDistributionMiniAEPForm.getCode());
					P.setLibelle(typeTableauDeDistributionMiniAEPForm.getLibelle());
					savedTypeTableauDeDistributionMiniAEP = typeTableauDeDistributionMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTableauDeDistributionMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeTableauDeDistributionMiniAEP")
    public int deleteTypeTableauDeDistributionMiniAEP(@Validated TypeTableauDeDistributionMiniAEPForm typeTableauDeDistributionMiniAEPForm,BindingResult bindingResult, Model model) {
		TypeTableauDeDistributionMiniAEP savedTypeTableauDeDistributionMiniAEP = new TypeTableauDeDistributionMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeTableauDeDistributionMiniAEP P = typeTableauDeDistributionMiniAEPRepository.findByCode(typeTableauDeDistributionMiniAEPForm.getCode());
					P.setIsDeleted(true);
					savedTypeTableauDeDistributionMiniAEP = typeTableauDeDistributionMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTableauDeDistributionMiniAEP.getCode();
		
        
    }
}
