package ne.mha.sinea.permission;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserForm {

	private int code;
	private String nom;
	private String prenom;
	private String login;
	private String motPasse;
	private int[] roles;
	
	
}
