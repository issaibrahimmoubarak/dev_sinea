package ne.mha.sinea.nomenclature.qualiteEauAep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class QualiteEauAepRestController {

	@Autowired
	QualiteEauAepRepository qualiteEauAepRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/qualiteEauAep")
	public int addQualiteEauAep(@Validated QualiteEauAepForm qualiteEauAepForm,BindingResult bindingResult, Model model) {
		QualiteEauAep savedQualiteEauAep = new QualiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					QualiteEauAep P = new QualiteEauAep();
					P.setLibelle(qualiteEauAepForm.getLibelle());
					savedQualiteEauAep = qualiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQualiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateQualiteEauAep")
    public int updateQualiteEauAep(@Validated QualiteEauAepForm qualiteEauAepForm,BindingResult bindingResult, Model model) {
		QualiteEauAep savedQualiteEauAep = new QualiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					QualiteEauAep P = qualiteEauAepRepository.findByCode(qualiteEauAepForm.getCode());
					P.setLibelle(qualiteEauAepForm.getLibelle());
					savedQualiteEauAep = qualiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQualiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteQualiteEauAep")
    public int deleteQualiteEauAep(@Validated QualiteEauAepForm qualiteEauAepForm,BindingResult bindingResult, Model model) {
		QualiteEauAep savedQualiteEauAep = new QualiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					QualiteEauAep P = qualiteEauAepRepository.findByCode(qualiteEauAepForm.getCode());
					P.setIsDeleted(true);
					savedQualiteEauAep = qualiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQualiteEauAep.getCode();
		
        
    }
}
