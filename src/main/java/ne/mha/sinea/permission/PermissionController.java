package ne.mha.sinea.permission;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class PermissionController {
	@Autowired
	RoleRepository roleService;
	@Autowired
	PrivilegeRepository privilegeService;
	@PreAuthorize("hasAuthority('gestion des accès')")
	//@PreAuthorize("hasRole('admin')")
	@GetMapping("/permission")
	public String  permission(Model model) {
		try{
			List<Role> roles = roleService.findByIsDeletedFalseOrderByLibelleAsc();
			List<Privilege> privileges = privilegeService.findByIsDeletedFalseOrderByLibelleAsc();
			
			model.addAttribute("roles", roles);
			model.addAttribute("privileges", privileges);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Autorisation d'accès");
			model.addAttribute("viewPath", "permission/permission");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
		

	}
		
	
}
