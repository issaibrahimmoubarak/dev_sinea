package ne.mha.sinea.nomenclature.marquePompe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MarquePompeRestController {

	@Autowired
	MarquePompeRepository marquePompeService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/marquePompe")
	public int addMarquePompe(@Validated MarquePompeForm marquePompeForm,BindingResult bindingResult, Model model) {
		MarquePompe savedMarquePompe = new MarquePompe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarquePompe P = new MarquePompe();
					P.setLibelle(marquePompeForm.getLibelle());
					savedMarquePompe = marquePompeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarquePompe.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateMarquePompe")
    public int updateMarquePompe(@Validated MarquePompeForm marquePompeForm,BindingResult bindingResult, Model model) {
		MarquePompe savedMarquePompe = new MarquePompe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarquePompe P = marquePompeService.findByCode(marquePompeForm.getCode());
					P.setLibelle(marquePompeForm.getLibelle());
					savedMarquePompe = marquePompeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarquePompe.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteMarquePompe")
    public int deleteMarquePompe(@Validated MarquePompeForm marquePompeForm,BindingResult bindingResult, Model model) {
		MarquePompe savedMarquePompe = new MarquePompe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarquePompe P = marquePompeService.findByCode(marquePompeForm.getCode());
					P.setIsDeleted(true);
					savedMarquePompe = marquePompeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarquePompe.getCode();
		
        
    }
}
