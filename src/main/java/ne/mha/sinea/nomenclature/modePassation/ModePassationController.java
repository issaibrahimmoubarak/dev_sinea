package ne.mha.sinea.nomenclature.modePassation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ne.mha.sinea.Template;

@Controller
public class ModePassationController {
	@Autowired
	ModePassationRepository modePassationService;
	
	//@PreAuthorize("hasAuthority('gestion des modePassations')")
	@GetMapping("/modePassation")
	public String  addModePassation(ModePassationForm modePassationForm, Model model) {
		try{
			List<ModePassation> modePassations = modePassationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("modePassations", modePassations);
	
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "nomenclature/modePassation");
			model.addAttribute("viewPath", "nomenclature/modePassation/modePassation");
			
		}
		catch(Exception e){
				
		}
		
		return Template.nomenclatureTemplate;

	}
	
		
}
