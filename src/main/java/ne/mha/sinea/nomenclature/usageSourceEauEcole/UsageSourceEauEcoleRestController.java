package ne.mha.sinea.nomenclature.usageSourceEauEcole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class UsageSourceEauEcoleRestController {

	@Autowired
	UsageSourceEauEcoleRepository usageSourceEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/usageSourceEauEcole")
	public int addUsageSourceEauEcole(@Validated UsageSourceEauEcoleForm usageSourceEauEcoleForm,BindingResult bindingResult, Model model) {
		UsageSourceEauEcole savedUsageSourceEauEcole = new UsageSourceEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					UsageSourceEauEcole P = new UsageSourceEauEcole();
					P.setLibelle(usageSourceEauEcoleForm.getLibelle());
					savedUsageSourceEauEcole = usageSourceEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUsageSourceEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateUsageSourceEauEcole")
    public int updateUsageSourceEauEcole(@Validated UsageSourceEauEcoleForm disponibiliteEauPVCForm,BindingResult bindingResult, Model model) {
		UsageSourceEauEcole savedUsageSourceEauEcole = new UsageSourceEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					UsageSourceEauEcole P = usageSourceEauEcoleRepository.findByCode(disponibiliteEauPVCForm.getCode());
					P.setLibelle(disponibiliteEauPVCForm.getLibelle());
					savedUsageSourceEauEcole = usageSourceEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUsageSourceEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteUsageSourceEauEcole")
    public int deleteUsageSourceEauEcole(@Validated UsageSourceEauEcoleForm usageSourceEauEcoleForm,BindingResult bindingResult, Model model) {
		UsageSourceEauEcole savedUsageSourceEauEcole = new UsageSourceEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					UsageSourceEauEcole P = usageSourceEauEcoleRepository.findByCode(usageSourceEauEcoleForm.getCode());
					P.setIsDeleted(true);
					savedUsageSourceEauEcole = usageSourceEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUsageSourceEauEcole.getCode();
		
        
    }
}
