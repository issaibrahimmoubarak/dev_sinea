package ne.mha.sinea.nomenclature.uniteMesure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UniteMesureForm {

	private int code;
	private String libelle;
	
}
