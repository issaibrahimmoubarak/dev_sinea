package ne.mha.sinea.nomenclature.typePrestation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypePrestationForm {

	private int code;
	private String libelle;
	
}
