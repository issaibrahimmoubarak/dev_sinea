package ne.mha.sinea.referentiel.reseauSuivi;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ReseauSuiviRepository extends CrudRepository<ReseauSuivi, Integer> {
	ReseauSuivi findByCode(Integer code);
	ReseauSuivi findByNom(String nom);
	List<ReseauSuivi> findByIsDeletedFalse();
	List<ReseauSuivi> findByIsDeletedTrue();

}
