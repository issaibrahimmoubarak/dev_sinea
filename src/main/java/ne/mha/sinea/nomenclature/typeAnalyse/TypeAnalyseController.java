package ne.mha.sinea.nomenclature.typeAnalyse;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TypeAnalyseController {

	@Autowired
	TypeAnalyseRepository typeAnalyseService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeAnalyse")
	public String  addTypeAnalyse(TypeAnalyseForm typeAnalyseForm, Model model) {
		try{
			List<TypeAnalyse> typeAnalyses = typeAnalyseService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeAnalyses", typeAnalyses);

			model.addAttribute("viewPath", "nomenclature/typeAnalyse/typeAnalyse");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
