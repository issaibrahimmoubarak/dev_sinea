package ne.mha.sinea.referentiel.indicateur.methodeCollecte;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MethodeCollecteForm {

	private int code;
	private String libelle;
	
}
