package ne.mha.sinea.nomenclature.typeSystemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeSystemeAEPRepository extends CrudRepository<TypeSystemeAEP, Integer> {
	TypeSystemeAEP findByCode(Integer code);
	TypeSystemeAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeSystemeAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeSystemeAEP> findByIsDeletedFalse();
	List<TypeSystemeAEP> findByIsDeletedTrue();
	List<TypeSystemeAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeSystemeAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
