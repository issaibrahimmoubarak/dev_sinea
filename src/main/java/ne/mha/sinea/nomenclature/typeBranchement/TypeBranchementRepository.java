package ne.mha.sinea.nomenclature.typeBranchement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeBranchementRepository extends CrudRepository<TypeBranchement, Integer> {
	TypeBranchement findByCode(Integer code);
	TypeBranchement findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeBranchement> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeBranchement> findByIsDeletedFalse();
	List<TypeBranchement> findByIsDeletedTrue();
	List<TypeBranchement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeBranchement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
