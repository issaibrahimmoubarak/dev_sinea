package ne.mha.sinea.nomenclature.materiaux;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface MateriauxRepository extends CrudRepository<Materiaux, Integer> {
	Materiaux findByCode(Integer code);
	Materiaux findByIsDeletedFalseAndLibelle(String libelle);
	List<Materiaux> findByIsDeletedFalseOrderByLibelleAsc();
	List<Materiaux> findByIsDeletedFalse();
	List<Materiaux> findByIsDeletedTrue();
	List<Materiaux> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Materiaux> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
