package ne.mha.sinea.nomenclature.typeReservoir;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeReservoirRepository extends CrudRepository<TypeReservoir, Integer> {
	TypeReservoir findByCode(Integer code);
	TypeReservoir findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeReservoir> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeReservoir> findByIsDeletedFalse();
	List<TypeReservoir> findByIsDeletedTrue();
	List<TypeReservoir> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeReservoir> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
