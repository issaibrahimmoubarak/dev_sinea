package ne.mha.sinea.nomenclature.typeInstallationMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeInstallationMiniAEPRestController {

	@Autowired
	TypeInstallationMiniAEPRepository typeInstallationMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeInstallationMiniAEP")
	public int addTypeInstallationMiniAEP(@Validated TypeInstallationMiniAEPForm typeInstallationMiniAEPForm,BindingResult bindingResult, Model model) {
		TypeInstallationMiniAEP savedTypeInstallationMiniAEP = new TypeInstallationMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeInstallationMiniAEP P = new TypeInstallationMiniAEP();
					P.setLibelle(typeInstallationMiniAEPForm.getLibelle());
					savedTypeInstallationMiniAEP = typeInstallationMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeInstallationMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeInstallationMiniAEP")
    public int updateTypeInstallationMiniAEP(@Validated TypeInstallationMiniAEPForm typeInstallationMiniAEPForm,BindingResult bindingResult, Model model) {
		TypeInstallationMiniAEP savedTypeInstallationMiniAEP = new TypeInstallationMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeInstallationMiniAEP P = typeInstallationMiniAEPRepository.findByCode(typeInstallationMiniAEPForm.getCode());
					P.setLibelle(typeInstallationMiniAEPForm.getLibelle());
					savedTypeInstallationMiniAEP = typeInstallationMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeInstallationMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeInstallationMiniAEP")
    public int deleteTypeInstallationMiniAEP(@Validated TypeInstallationMiniAEPForm typeInstallationMiniAEPForm,BindingResult bindingResult, Model model) {
		TypeInstallationMiniAEP savedTypeInstallationMiniAEP = new TypeInstallationMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeInstallationMiniAEP P = typeInstallationMiniAEPRepository.findByCode(typeInstallationMiniAEPForm.getCode());
					P.setIsDeleted(true);
					savedTypeInstallationMiniAEP = typeInstallationMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeInstallationMiniAEP.getCode();
		
        
    }
}
