package ne.mha.sinea.nomenclature.typeAnalyse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeAnalyseForm {

	private int code;
	private String libelle;
	
}
