package ne.mha.sinea.nomenclature.typeTableauDeDistributionMiniAEP;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeTableauDeDistributionMiniAEPForm {

	private int code;
	private String libelle;
	
}
