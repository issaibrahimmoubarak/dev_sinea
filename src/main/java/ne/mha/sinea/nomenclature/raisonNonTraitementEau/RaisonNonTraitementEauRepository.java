package ne.mha.sinea.nomenclature.raisonNonTraitementEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface RaisonNonTraitementEauRepository extends CrudRepository<RaisonNonTraitementEau, Integer> {
	RaisonNonTraitementEau findByCode(Integer code);
	RaisonNonTraitementEau findByIsDeletedFalseAndLibelle(String libelle);
	List<RaisonNonTraitementEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<RaisonNonTraitementEau> findByIsDeletedFalse();
	List<RaisonNonTraitementEau> findByIsDeletedTrue();
	List<RaisonNonTraitementEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<RaisonNonTraitementEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
