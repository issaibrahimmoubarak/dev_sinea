package ne.mha.sinea.nomenclature.typeOuvrageAssainissement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeOuvrageAssainissementForm {

	private int code;
	private String libelle;
	
}
