package ne.mha.sinea.permission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class RoleRestController {

	@Autowired
	RoleRepository roleService;
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/role")
	public int addRole(@Validated RoleForm roleForm,BindingResult bindingResult, Model model) {
		Role savedRole = new Role();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Role P = new Role();
				P.setLibelle(roleForm.getLibelle());
				savedRole = roleService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRole.getCode();
		
        
    }
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/updateRole")
    public int updateRole(@Validated RoleForm roleForm,BindingResult bindingResult, Model model) {
		Role savedRole = new Role();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Role P = roleService.findByCode(roleForm.getCode());
				P.setLibelle(roleForm.getLibelle());
				savedRole = roleService.save(P);
				
				}
			catch(Exception e){
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRole.getCode();
		
        
    }
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/deleteRole")
    public int deleteRole(@Validated RoleForm roleForm,BindingResult bindingResult, Model model) {
		Role savedRole = new Role();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Role P = roleService.findByCode(roleForm.getCode());
				P.setIsDeleted(true);
				savedRole = roleService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRole.getCode();
		
        
    }
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@GetMapping("/getRole/{code}")
	public String  listeRole(@PathVariable("code") int code) {
		String res = "";
		try{
			
			Role role = roleService.findByCode(code);
			res += ",{ id:"+role.getCode()+", pId:0, name:\""+role.getLibelle()+"\"}";
			res = "[{ id:0, pId:0, name:\"/\", open:true}"+res+"]";
			
			}
		catch(Exception e){
				
			}
		return res;
	}
	
	
	
}
