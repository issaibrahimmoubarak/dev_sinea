package ne.mha.sinea.pem;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface EquipementForageRepository extends CrudRepository<EquipementForage, Integer> {
	
	EquipementForage findByCode(Integer code);
	List<EquipementForage> findByIsDeletedFalse();
	
}