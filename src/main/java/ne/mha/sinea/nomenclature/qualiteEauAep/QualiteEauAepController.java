package ne.mha.sinea.nomenclature.qualiteEauAep;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class QualiteEauAepController {

	@Autowired
	QualiteEauAepRepository qualiteEauAepRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/qualiteEauAep")
	public String  addQualiteEauAep(QualiteEauAepForm qualiteEauAepForm, Model model) {
		try{
			List<QualiteEauAep> qualiteEauAep = qualiteEauAepRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("qualiteEauAEP", qualiteEauAep);

			model.addAttribute("viewPath", "nomenclature/qualiteEauAep/qualiteEauAep");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
