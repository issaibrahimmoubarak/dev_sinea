package ne.mha.sinea.nomenclature.typeUsage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeUsageRestController {

	@Autowired
	TypeUsageRepository typeUsageService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeUsage")
	public int addTypeUsage(@Validated TypeUsageForm typeUsageForm,BindingResult bindingResult, Model model) {
		TypeUsage savedTypeUsage = new TypeUsage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeUsage P = new TypeUsage();
					P.setLibelle(typeUsageForm.getLibelle());
					savedTypeUsage = typeUsageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeUsage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeUsage")
    public int updateTypeUsage(@Validated TypeUsageForm typeUsageForm,BindingResult bindingResult, Model model) {
		TypeUsage savedTypeUsage = new TypeUsage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeUsage P = typeUsageService.findByCode(typeUsageForm.getCode());
					P.setLibelle(typeUsageForm.getLibelle());
					savedTypeUsage = typeUsageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeUsage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeUsage")
    public int deleteTypeUsage(@Validated TypeUsageForm typeUsageForm,BindingResult bindingResult, Model model) {
		TypeUsage savedTypeUsage = new TypeUsage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeUsage P = typeUsageService.findByCode(typeUsageForm.getCode());
					P.setIsDeleted(true);
					savedTypeUsage = typeUsageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeUsage.getCode();
		
        
    }
}
