package ne.mha.sinea.referentiel.indicateur.indicateurClassification;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface IndicateurClassificationRepository extends CrudRepository<IndicateurClassification, IndicateurClassificationKey>  {

	List<IndicateurClassification> findByIsDeletedFalse();
	List<IndicateurClassification> findByIsDeletedTrue();
	List<IndicateurClassification> findByIsDeletedFalseAndClassification_Code(Integer code);
	IndicateurClassification findByIsDeletedFalseAndIndicateur_CodeAndClassification_Code(Integer codeIndicateur, Integer codeClassification);
	
}
