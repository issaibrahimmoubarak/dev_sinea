package ne.mha.sinea.referentiel.lieuPublic;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface LieuPublicRepository extends CrudRepository<LieuPublic, Integer> {
	LieuPublic findByCode(Integer code);
	LieuPublic findBydenomination(String denomination);
	List<LieuPublic> findByIsDeletedFalse();
	List<LieuPublic> findByIsDeletedTrue();
}
