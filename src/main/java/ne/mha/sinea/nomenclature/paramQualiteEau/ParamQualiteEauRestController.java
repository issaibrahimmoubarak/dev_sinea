package ne.mha.sinea.nomenclature.paramQualiteEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ParamQualiteEauRestController {

	@Autowired
	ParamQualiteEauRepository paramQualiteEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/paramQualiteEau")
	public int addParamQualiteEau(@Validated ParamQualiteEauForm paramQualiteEauForm,BindingResult bindingResult, Model model) {
		ParamQualiteEau savedParamQualiteEau = new ParamQualiteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParamQualiteEau P = new ParamQualiteEau();
					P.setLibelle(paramQualiteEauForm.getLibelle());
					savedParamQualiteEau = paramQualiteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParamQualiteEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateParamQualiteEau")
    public int updateParamQualiteEau(@Validated ParamQualiteEauForm paramQualiteEauForm,BindingResult bindingResult, Model model) {
		ParamQualiteEau savedParamQualiteEau = new ParamQualiteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParamQualiteEau P = paramQualiteEauRepository.findByCode(paramQualiteEauForm.getCode());
					P.setLibelle(paramQualiteEauForm.getLibelle());
					savedParamQualiteEau = paramQualiteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParamQualiteEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteParamQualiteEau")
    public int deleteParamQualiteEau(@Validated ParamQualiteEauForm paramQualiteEauForm,BindingResult bindingResult, Model model) {
		ParamQualiteEau savedParamQualiteEau = new ParamQualiteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParamQualiteEau P = paramQualiteEauRepository.findByCode(paramQualiteEauForm.getCode());
					P.setIsDeleted(true);
					savedParamQualiteEau = paramQualiteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParamQualiteEau.getCode();
		
        
    }
}
