package ne.mha.sinea.nomenclature.typeAnalyse;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeAnalyseRepository extends CrudRepository<TypeAnalyse, Integer> {
	TypeAnalyse findByCode(Integer code);
	TypeAnalyse findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeAnalyse> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeAnalyse> findByIsDeletedFalse();
	List<TypeAnalyse> findByIsDeletedTrue();
	List<TypeAnalyse> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeAnalyse> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
