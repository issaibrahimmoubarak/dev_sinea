package ne.mha.sinea.nomenclature.typeExhaure;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeExhaureRepository extends CrudRepository<TypeExhaure, Integer> {
	TypeExhaure findByCode(Integer code);
	TypeExhaure findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeExhaure> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeExhaure> findByIsDeletedFalse();
	List<TypeExhaure> findByIsDeletedTrue();
	List<TypeExhaure> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeExhaure> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
