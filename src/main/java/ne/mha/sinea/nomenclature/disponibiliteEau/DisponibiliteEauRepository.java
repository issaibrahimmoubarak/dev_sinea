package ne.mha.sinea.nomenclature.disponibiliteEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface DisponibiliteEauRepository extends CrudRepository<DisponibiliteEau, Integer> {
	DisponibiliteEau findByCode(Integer code);
	DisponibiliteEau findByIsDeletedFalseAndLibelle(String libelle);
	List<DisponibiliteEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<DisponibiliteEau> findByIsDeletedFalse();
	List<DisponibiliteEau> findByIsDeletedTrue();
	List<DisponibiliteEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<DisponibiliteEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
