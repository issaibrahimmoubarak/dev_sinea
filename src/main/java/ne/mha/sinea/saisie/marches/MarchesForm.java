package ne.mha.sinea.saisie.marches;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarchesForm {

    private String objet;
    private Date anneeMarchesPublique;
	private double coutPrevisionnel;
	private Date dateLancementProcedure;
	private Date dateAttributionContrat;
	private Date dateDemarragePrestation;
	private Date dateAchevementPrestation;
    private Integer codeIntervenant;
    private Integer codeTypeMarches;
    private Integer codeFinancement;
    private Integer codeModePassation;
}
