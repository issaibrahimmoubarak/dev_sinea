package ne.mha.sinea.referentiel.indicateur.ficheTechnique;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;


//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "fiche_technique")
public class FicheTechnique extends CommonProperties  {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "code")
		private Integer code;
		
		@Column(name = "definition",columnDefinition="TEXT")
		private String definition;
		
		@Column(name = "donnees_requises",columnDefinition="TEXT")
		private String donneesRequises;
		
		@Column(name = "methode_calcul",columnDefinition="TEXT")
		private String methodeCalcul;
		
		@Column(name = "commentaire_limite",columnDefinition="TEXT")
		private String commentaireLimite;
		
		@Column(name = "periodicite")
		private String periodicite;
		
		@Column(name = "niveau_desagregation",columnDefinition="TEXT")
		private String niveauDesagregation;
		
		@JoinColumn(name = "code_indicateur", referencedColumnName = "code")
		@ManyToOne
		private Indicateur indicateur;
		
		/*@JoinColumn(name = "code_unite", referencedColumnName = "code")
		@ManyToOne
		private Unite unite;*/
		
		@Column(name = "unite",columnDefinition="TEXT")
		private String unite;
		
		/*@JoinColumn(name = "code_source", referencedColumnName = "code")
		@ManyToOne
		private Source source;*/
		
		@Column(name = "source",columnDefinition="TEXT")
		private String source;
		
		/*@JoinColumn(name = "code_methode_collecte", referencedColumnName = "code")
		@ManyToOne
		private MethodeCollecte methodeCollecte;*/
		
		@Column(name = "methode_collecte",columnDefinition="TEXT")
		private String methodeCollecte;
			
}
