package ne.mha.sinea.referentiel.projet;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;

import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import ne.mha.sinea.Template;

@Controller
public class ProjetController {

	@Autowired
	ProjetRepository projetService;

	// @PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/projet")
	public String addProjet(ProjetForm projetForm, Model model) {
		try {
			List<Projet> projets = projetService.findByIsDeletedFalse();
			model.addAttribute("projets", projets);
			model.addAttribute("viewPath", "referentiel/projet/projet");
		} catch (Exception e) {

		}

		return Template.secondTemplate;

	}

	// validation du formulaire d'ajout projet
	@PostMapping("/projet")
	public String addProjetsSubmit(@Valid ProjetForm projetForm, BindingResult bindingResult, Model model) {
		// s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {
			// if(true){
			try {
				// récuperer les données saisies
				Projet P = new Projet();
				P.setLibelle(projetForm.getLibelle());
				P.setZoneIntervention(projetForm.getZoneIntervention());
				P.setDateDebut(projetForm.getDateDebut());
				P.setDateFin(projetForm.getDateFin());
				P.setFinancement(projetForm.getFinancement());
				P.setMontant(projetForm.getMontant());
				P.setMaitreOuvrage(projetForm.getMaitreOuvrage());
				P.setMaitreOeuvre(projetForm.getMaitreOeuvre());

				projetService.save(P);

				// indiquer que l'operation d'ajout a réussie

				List<Projet> projets = projetService.findByIsDeletedFalseOrderByLibelleAsc();
				model.addAttribute("projets", projets);

				model.addAttribute("viewPath", "referentiel/projet/projet");

			} catch (Exception e) {
				System.out.println("errer" + e);
			}

		} else {
			try {

			} catch (Exception e) {
				System.out.println("errer" + e);
			}
		}

		// afficher le template avec la vue (formulaire) à l'intérieur
		return Template.secondTemplate;

	}

	// modification d'une projet
	@GetMapping("/updateProjet/{code}")
	public String updateProjet(@PathVariable("code") int code,
			ProjetForm projetForm, Model model) {
		try {
			// récuperer les informations du projet à modifier
			Projet projet = projetService.findByCode(code);
			// envoie des informations du projet à modifier à travers le modele
			projetForm.setLibelle(projet.getLibelle());
			projetForm.setZoneIntervention(projet.getZoneIntervention());
			projetForm.setDateDebut(projet.getDateDebut());
			projetForm.setDateFin(projet.getDateFin());
			projetForm.setFinancement(projet.getFinancement());
			projetForm.setMontant(projet.getMontant());
			projetForm.setMaitreOuvrage(projet.getMaitreOuvrage());
			projetForm.setMaitreOeuvre(projet.getMaitreOeuvre());
			// récuperation de la liste des projet de la base de données
			List<Projet> projets = projetService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("projets", projets);

			model.addAttribute("viewPath", "referentiel/projet/updateProjet");

		} catch (Exception e) {

		}
		return Template.secondTemplate;

	}

	@PostMapping("/updateProjet/{code}")
	public RedirectView updateProjetSubmit(@Validated ProjetForm projetForm, BindingResult bindingResult,
			@PathVariable("code") int code, Model model, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/projet", true);
		if (!bindingResult.hasErrors()) {
			try {
				// recupérer les informations saisies
				Projet P = projetService.findByCode(code);
				P.setLibelle(projetForm.getLibelle());
				P.setZoneIntervention(projetForm.getZoneIntervention());
				P.setDateDebut(projetForm.getDateDebut());
				P.setDateFin(projetForm.getDateFin());
				P.setFinancement(projetForm.getFinancement());
				P.setMontant(projetForm.getMontant());
				P.setMaitreOuvrage(projetForm.getMaitreOuvrage());
				P.setMaitreOeuvre(projetForm.getMaitreOeuvre());
				// enregistrer les informations dans la base de données
				projetService.save(P);

				// indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");

			} catch (Exception e) {
				// indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
		} else {

		}

		// redirection vers la page d'ajout
		return redirectView;

	}

	@GetMapping("/deleteProjet/{code}")
	public RedirectView deleteProjet(@PathVariable("code") int code, Model model,
			RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/projet", true);
		try {
			Projet P = projetService.findByCode(code);
			P.setIsDeleted(true);
			projetService.save(P);
			;

			// indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		} catch (Exception e) {
			// indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		// redirection vers la page d'ajout
		return redirectView;

	}

}
