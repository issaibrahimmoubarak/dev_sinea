package ne.mha.sinea.referentiel.indicateur.sousGroupe;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface SousGroupeRepository extends CrudRepository<SousGroupe, Integer> {
	SousGroupe findByCode(Integer code);
	SousGroupe findByIsDeletedFalseAndLibelle(String libelle);
	List<SousGroupe> findByIsDeletedFalseOrderByLibelleAsc();
	List<SousGroupe> findByIsDeletedFalse();
	List<SousGroupe> findByIsDeletedTrue();
	List<SousGroupe> findByIsDeletedFalseAndParent_Code(Integer code);
	List<SousGroupe> findByIsDeletedFalseAndParent_Libelle(String libelle);
	List<SousGroupe> findByIsDeletedFalseAndNiveau(int niveau);
	List<SousGroupe> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<SousGroupe> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	

}
