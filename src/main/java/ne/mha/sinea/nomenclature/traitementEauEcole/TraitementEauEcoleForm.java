package ne.mha.sinea.nomenclature.traitementEauEcole;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TraitementEauEcoleForm {

	private int code;
	private String libelle;
	
}
