package ne.mha.sinea.referentiel.indicateur.methodeCollecte;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrame;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrameRepository;

@Controller
public class MethodeCollecteController {
	@Autowired
	MethodeCollecteRepository methodeCollecteService;
	@Autowired
	TypeClassificationRepository typeClassificationService;
	@Autowired
	SectionFrameRepository sectionFrameService;
	//@PreAuthorize("hasAuthority('gestion des méthodes de collecte')")
	@GetMapping("/methodeCollecte")
	public String  addMethodeCollecte(MethodeCollecteForm methodeCollecteForm, Model model) {
		try{
			List<MethodeCollecte> methodeCollectes = methodeCollecteService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("methodeCollectes", methodeCollectes);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Méthode de collecte");
			model.addAttribute("viewPath", "referentiel/methodeCollecte/methodeCollecte");
			
		}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
