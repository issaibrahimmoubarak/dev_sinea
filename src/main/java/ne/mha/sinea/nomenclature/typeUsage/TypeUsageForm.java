package ne.mha.sinea.nomenclature.typeUsage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeUsageForm {

	private int code;
	private String libelle;
	
}
