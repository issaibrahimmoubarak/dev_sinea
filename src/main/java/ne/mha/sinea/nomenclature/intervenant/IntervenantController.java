package ne.mha.sinea.nomenclature.intervenant;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
// import org.springframework.validation.BindingResult;
// import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
// import org.springframework.web.servlet.mvc.support.RedirectAttributes;
// import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeIntervenant.TypeIntervenant;
import ne.mha.sinea.nomenclature.typeIntervenant.TypeIntervenantRepository;

@Controller
public class IntervenantController {

	@Autowired
	EntityManager em;

	@Autowired
	IntervenantRepository intervenantService;
	@Autowired
	TypeIntervenantRepository typeIntervenantService;

	// formulaire intervenant dont l'ajout , l'import et l'export
	// @PreAuthorize("hasAuthority('Ajout Intervenant'))
	@GetMapping("/intervenant")
	public String addIntervenant(IntervenantForm intervenantForm, Model model) {
		try {
			List<TypeIntervenant> typeIntervenant = typeIntervenantService.findByIsDeletedFalse();
			model.addAttribute("typeIntervenant", typeIntervenant);
			List<Intervenant> intervenant = intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenant", intervenant);
			// List<Intervenant> intervenants = (List<Intervenant>)
			// intervenantService.findByIsDeletedFalse();
			// model.addAttribute("intervenants", intervenants);
			// List<TypeIntervenant> typeIntervenants =
			// typeIntervenantService.findByIsDeletedFalse();
			// model.addAttribute("typeIntervenants", typeIntervenants);

			model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");

		} catch (Exception e) {

		}
		return Template.secondTemplate;
	}

	// validation du formulaire d'ajout intervenant
	@PostMapping("/intervenant")
	public String addIntervenantsSubmit(@Validated IntervenantForm intervenantForm, BindingResult bindingResult,
			Model model) {
		// s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {

			try {
				// récuperer les données saisies
				Intervenant I = new Intervenant();
				I.setDenomination(intervenantForm.getDenomination());
				I.setTypeIntervenant(typeIntervenantService.findByCode(intervenantForm.getCodeTypeIntervenant()));
				intervenantService.save(I);

				// indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				List<TypeIntervenant> typeIntervenant = typeIntervenantService.findByIsDeletedFalse();
				model.addAttribute("typeIntervenant", typeIntervenant);
				List<Intervenant> intervenant = intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenant", intervenant);
				model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");

			} catch (Exception e) {

			}

		} else {
			try {

			} catch (Exception e) {

			}
		}

		// afficher le template avec la vue (formulaire) à l'intérieur
		return Template.secondTemplate;

	}

	// modification d'une intervenant
	@GetMapping("/updateIntervenant/{code}")
	public String updateIntervenant(@PathVariable("code") int code,
			IntervenantForm intervenantForm, Model model) {
		try {
			// récuperer les informations de la intervenant à modifier
			Intervenant intervenants = intervenantService.findByCode(code);
			// envoie des informations de la intervenant à modifier à travers le modele
			intervenantForm.setDenomination(intervenants.getDenomination());
			intervenantForm.setCodeTypeIntervenant(intervenants.getTypeIntervenant().getCode());

			// récuperation de la liste des intervenant de la base de données
			List<TypeIntervenant> typeIntervenant = typeIntervenantService.findByIsDeletedFalse();
			model.addAttribute("typeIntervenant", typeIntervenant);
			List<Intervenant> intervenant = intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenant", intervenant);

			// model.addAttribute("horizontalMenu", "horizontalMenu");
			// model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			// model.addAttribute("breadcrumb", "breadcrumb");
			// model.addAttribute("navigationPath", "Nomenclature/Intervenant");
			model.addAttribute("viewPath", "nomenclature/intervenant/updateIntervenant");

		} catch (Exception e) {

		}
		return Template.secondTemplate;

	}

	@PostMapping("/updateIntervenant/{code}")
	public RedirectView updateIntervenantSubmit(@Validated IntervenantForm intervenantForm, BindingResult bindingResult,
			@PathVariable("code") int code, Model model, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/intervenant", true);
		if (!bindingResult.hasErrors()) {
			try {
				// recupérer les informations saisies
				Intervenant I = intervenantService.findByCode(code);
				I.setDenomination(intervenantForm.getDenomination());
				I.setTypeIntervenant(typeIntervenantService.findByCode(intervenantForm.getCodeTypeIntervenant()));
				// enregistrer les informations dans la base de données
				intervenantService.save(I);

				// indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");

			} catch (Exception e) {
				// indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
		} else {

		}

		// redirection vers la page d'ajout
		return redirectView;

	}

	@GetMapping("/deleteIntervenant/{code}")
	public RedirectView deleteIntervenant(@PathVariable("code") int code, Model model,
			RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/intervenant", true);
		try {
			Intervenant I = intervenantService.findByCode(code);
			I.setIsDeleted(true);
			intervenantService.save(I);
			;

			// indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		} catch (Exception e) {
			// indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		// redirection vers la page d'ajout
		return redirectView;

	}

}
