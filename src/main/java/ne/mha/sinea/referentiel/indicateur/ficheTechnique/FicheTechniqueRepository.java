package ne.mha.sinea.referentiel.indicateur.ficheTechnique;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface FicheTechniqueRepository extends CrudRepository<FicheTechnique, Integer> {
	FicheTechnique findByCode(Integer code);
	FicheTechnique findByIsDeletedFalseAndIndicateur_Libelle(String libelle);
	FicheTechnique findByIsDeletedFalseAndIndicateur_Code(Integer code);
	List<FicheTechnique> findByIsDeletedFalse();
	List<FicheTechnique> findByIsDeletedTrue();
	

}
