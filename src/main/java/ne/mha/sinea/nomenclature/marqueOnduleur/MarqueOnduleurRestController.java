package ne.mha.sinea.nomenclature.marqueOnduleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MarqueOnduleurRestController {

	@Autowired
	MarqueOnduleurRepository marqueOnduleurRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/marqueOnduleur")
	public int addMarqueOnduleur(@Validated MarqueOnduleurForm marqueOnduleurForm,BindingResult bindingResult, Model model) {
		MarqueOnduleur savedMarqueOnduleur = new MarqueOnduleur();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarqueOnduleur P = new MarqueOnduleur();
					P.setLibelle(marqueOnduleurForm.getLibelle());
					savedMarqueOnduleur = marqueOnduleurRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueOnduleur.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateMarqueOnduleur")
    public int updateMarqueOnduleur(@Validated MarqueOnduleurForm marqueOnduleurForm,BindingResult bindingResult, Model model) {
		MarqueOnduleur savedMarqueOnduleur = new MarqueOnduleur();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarqueOnduleur P = marqueOnduleurRepository.findByCode(marqueOnduleurForm.getCode());
					P.setLibelle(marqueOnduleurForm.getLibelle());
					savedMarqueOnduleur = marqueOnduleurRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueOnduleur.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteMarqueOnduleur")
    public int deleteMarqueOnduleur(@Validated MarqueOnduleurForm marqueOnduleurForm,BindingResult bindingResult, Model model) {
		MarqueOnduleur savedMarqueOnduleur = new MarqueOnduleur();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MarqueOnduleur P = marqueOnduleurRepository.findByCode(marqueOnduleurForm.getCode());
					P.setIsDeleted(true);
					savedMarqueOnduleur = marqueOnduleurRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueOnduleur.getCode();
		
        
    }
}
