package ne.mha.sinea.saisie.ouvragesAssainissement;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OuvragesAssainissementForm {

	private String codeINS;
	private String indentification;
	private int nbDispositifInterieur;
	private int nbDispositifEnceinte;
	private Integer codeLieu;
	private Integer codeTypeLatrines;
	private Integer codeTypeOuvrageAssainissement;
	
	// private String lieuEcole;
	// private String lieuMenage;
	// private String lieuMarches;
	// private String lieuCulte;
	// private String lieuGareRoutier;
	// private Boolean typeLatrineManuelle;
	// private Boolean typeLatrineMecanique;
	// private Boolean typeLatrineEcoSan;
	// private Boolean typeLatrineSanPlat;
	// private Boolean typeLatrineUneFosse;
	// private Boolean typeLatrineDoubleFosse;
	// private Boolean typeOuvrageAssaiLatrine;
	// private Boolean typeOuvrageAssaiBac;
	// private Boolean typeOuvrageAssaiDispo;
	// private Boolean typeOuvrageAssaiDouche;
	// private Boolean typeOuvrageAssaiUri;
	// private Boolean typeOuvrageAssaiDp;
	// private Boolean typeOuvrageAssaiStation;
	// // private Boolean typeOuvrageAssaiAutre;
	// private Boolean typeOuvrageAssaiPrecision;
	
}
