package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DisponibiliteEauPVCForm {

	private int code;
	private String libelle;
	
}
