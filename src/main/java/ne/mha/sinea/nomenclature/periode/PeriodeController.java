package ne.mha.sinea.nomenclature.periode;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrame;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrameRepository;

@Controller
public class PeriodeController {
	@Autowired
	PeriodeRepository periodeService;
	@Autowired
	TypeClassificationRepository typeClassificationService;
	@Autowired
	SectionFrameRepository sectionFrameService;
	
	//@PreAuthorize("hasAuthority('gestion des périodes')")
	@GetMapping("/periode")
	public String  addPeriode(PeriodeForm periodeForm, Model model) {
		try{
			List<Periode> periodes = periodeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("periodes", periodes);
			
			/*List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);*/

			model.addAttribute("viewPath", "nomenclature/periode/periode");
			
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
	
}
