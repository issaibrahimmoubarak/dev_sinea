package ne.mha.sinea.nomenclature.typeOuvrageAssainissement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeOuvrageAssainissementRestController {

	@Autowired
	TypeOuvrageAssainissementRepository typeOuvrageAssainissementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeOuvrageAssainissement")
	public int addTypeOuvrageAssainissement(@Validated TypeOuvrageAssainissementForm typeOuvrageAssainissementForm,BindingResult bindingResult, Model model) {
		TypeOuvrageAssainissement savedTypeOuvrageAssainissement = new TypeOuvrageAssainissement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeOuvrageAssainissement P = new TypeOuvrageAssainissement();
					P.setLibelle(typeOuvrageAssainissementForm.getLibelle());
					savedTypeOuvrageAssainissement = typeOuvrageAssainissementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeOuvrageAssainissement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeOuvrageAssainissement")
    public int updateTypeOuvrageAssainissement(@Validated TypeOuvrageAssainissementForm typeOuvrageAssainissementForm,BindingResult bindingResult, Model model) {
		TypeOuvrageAssainissement savedTypeOuvrageAssainissement = new TypeOuvrageAssainissement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeOuvrageAssainissement P = typeOuvrageAssainissementRepository.findByCode(typeOuvrageAssainissementForm.getCode());
					P.setLibelle(typeOuvrageAssainissementForm.getLibelle());
					savedTypeOuvrageAssainissement = typeOuvrageAssainissementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeOuvrageAssainissement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeOuvrageAssainissement")
    public int deleteTypeOuvrageAssainissement(@Validated TypeOuvrageAssainissementForm typeOuvrageAssainissementForm,BindingResult bindingResult, Model model) {
		TypeOuvrageAssainissement savedTypeOuvrageAssainissement = new TypeOuvrageAssainissement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeOuvrageAssainissement P = typeOuvrageAssainissementRepository.findByCode(typeOuvrageAssainissementForm.getCode());
					P.setIsDeleted(true);
					savedTypeOuvrageAssainissement = typeOuvrageAssainissementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeOuvrageAssainissement.getCode();
		
        
    }
}
