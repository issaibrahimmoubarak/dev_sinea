package ne.mha.sinea.referentiel.zone;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ZoneRepository extends CrudRepository<Zone, Integer> {
	Zone findByCode(Integer code);
	Zone findByIsDeletedFalseAndLibelle(String libelle);
	Zone findByIsDeletedFalseAndLibelleAndParent_Libelle(String libelle,String libelleParent);
	List<Zone> findByIsDeletedFalseOrderByLibelleAsc();
	List<Zone> findByIsDeletedFalseAndParent_code(Integer code);
	List<Zone> findByParent_libelle(String libelle);
	List<Zone> findByIsDeletedFalseAndNiveau(Integer niveau);
	List<Zone> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Zone> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
}
